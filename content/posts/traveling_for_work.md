---
title: "Traveling for work"
date: 2023-04-13
tags: 
- "career"
- "personal"
featured_image: https://dl.dropboxusercontent.com/s/l3ec00tm4vbvs0a/business-travel.jpeg
type: post
---

One of the things I dreamed of was being able to travel a lot, I did a couple of times
travel to conferences with expenses paid, it was nice meeting new people, walking through
the city because I was there for the conference days but... I'm not required at the conference
site unless I'm speaking so, I took the time to go around the place when there were no
interesting talks.

Traveling for work is quite different and definitely not like that, no time for anything but work!

# The reality
I'm now in a project in which I have to travel between Dubai (UAE) and Riyadh (KSA) every week,
YES, every week I'm taking a 6:00 flight on Sunday, landing in Riyadh at 7:00 (local time), heading
to the hotel, quick shower, run to the client, then on Wednesday, take the 21:15 flight to Dubai,
land at 00:00 (local time) and reach home at around 2:00 to then start working again by 9:00 on
Thursday, fortunately I can work from home.

It is quite tough because in Saudi Arabia, the work week is from Sunday to Thursday while in Dubai
it was changes to Monday to Friday, so, the only day I get to spend with my whole family is Saturday
because our daughter is at school on Friday.

Now, I thought it was going to be cool, I got to stay at hotels, buffet breakfast every day, all
meals paid, I would go out of the office and go walk around but... again... nope. One thing is
having that continental breakfast or eating those buffet waffles every only when you're on vacation,
and a complete different thing is having all that "vacation food" every day, you need to watch what
you eat, or else, you'll gain weight at the speed of light and, you know, gaining weight is way easier
than losing it. 

Then, when you're done with the work at client site, it's time to rush to the hotel to prepare stuff
for the next day, sometimes we need to run a workshop, present some findings to the client, have some
internal meeting to discuss the next steps, all of this happens at the hotel, after hours, it usually
runs until around 22:00, when everyone goes back to their hotel and go sleep. For me, if I don't have
any internal meeting, I always have something to prepare, some meeting recordings to hear and take notes
or a very large technical document to review and comment on, so, no time to hike around either.

There are also long hours at the airport waiting for the flights, I thought "yeah, that's cool, I have
this credit card that give me access to the airport lounge", oh hell no... it turns out a lot of people
also have the same benefit with different credit cards and those cool airport lounges are always crowded,
more often than not it's better to go in, grab some water or snacks and then go out, most of the time
there's no place to sit, credit card companies should stop selling this as a cool feature on the cards
and find something else to add value to people's travel.

The only thing that doesn't disappoint the fantasy of traveling frequently is meeting new people, I've
meet quite a few very cool Saudis at the client.

Well, I guess there's nothing perfect in the world, but this made me realize I need to make a more
conscious effort to use my time better, I guess I'll write about it later, maybe I should reread
*Master your time, Master your life* by Brian Tracy...

Let's see where this new journey takes me...

