---
title: "And so... I left Careem"
date: 2023-04-08
tags: 
- "career"
- "personal"
featured_image: https://dl.dropboxusercontent.com/s/1co518dzlq6fk78/header.jpeg
type: post
---

I haven't blogged in a while, to be honest, I didn't have too much to tell and
I wasn't feeling like, telling anything, I was feeling stuck and I wasn't 100%
happy with what I was doing anymore, yeah, very strong statements for someone
who was an advocate of working at Careem and I even used to say *I will leave
Careem when I leave Dubai*, which clearly wasn't the case.

Don't get me wrong, I still think Careem is one of the best places to work in the
region, I was very happy to be part of the team and a lot of the achievements during
the 5 years or so I was there, if you're looking for a job in the Middle East, Careem
is one of the places you want look at.

Then... if it's so awesome, why did I leave?

Experience and life has taught me that when you start seeing things you don't like
it's not good to ignore them, I've done that before and didn't turn out well for me.
Even if other people were OK with stuff I didn't like, it's my standard and it's
my way of seeing things what should guide me, not what other people think is good
or bad, I have my own moral compass to follow, so... for me...

## It all started with...
COVID!, yes, COVID was one of the best things that happened to me and I often feel guilty
when I say this. Yes, it was terrible in a sense but it forced technology and organizations
evolve and embrace remote ways of working, which was awesome and a dream come true for me,
working from home was something I dreamed of since I graduated and it was possible thanks
to a global pandemic, sounds blunt but... it's true, for a lot of tech workers this
opened the door of remote work and I was one of those.

It was also a time of uncertainty for the whole industry, Careem had to let go a bit
over 700 people, and many other companies did much worse. We embarked on a cost-cutting
journey and switched off some services, reduced capacity on others and made some extra
efforts on getting the most out of the resources we were using, so after some time we were
running almost the same infrastructure at a lower cost thanks to a lot of optimizations the
teams did. So, I survived the first crisis and all the Senior leadership at the company
were quite optimist we were going to go through the whole crisis without laying more colleagues
off.

Until... some money we were expecting from our parent company was delayed, Senior management
said it was nothing to worry about, thanks to the costs we were able to reduce we could resist
until the funds were released to us, there were not going to be more layoffs and there were not
going to be offers rescinded even though we were going into a hiring freeze...

## But there were...
LinkedIn was flooded with posts from people whose offers were rescinded by a lot of companies,
including Careem and, since there was hiring freeze, our Talent Acquisition team was being impacted
with quite a few layoffs, so, Senior leadership was doing exactly the opposite. This, to me, was the
first red flag.

## Then the Russia-Ukraine war...
This marked a second crisis and there were layoffs all over the place, Careem said "not this time" but, 
given the previous events I didn't trust and I felt like walking on a tightrope ever since. On the other
hand inflation was hiking and on an All Hands meeting the compensation team say "there's going to be
some adjusts in the salaries to compensate the current inflation rate". Time passed and a lot of my colleagues
got an increase, but not me. I asked one of the leads for the concerning HR team and the answer was:

> We never said we were going to compensate the inflation rate, the increases were to ensure we were 
> still competitive in the market, your salary is already competitive, hence, no increase.

This was the second red flag and it was when I started interviewing, it wasn't about not getting the increase,
it was about the fact that they said something and then took it back and claimed that's not what they said,
which is equivalent to saying "we won't rescind offers or lay people off" and then rescinding offers and lay
people off...

## Ultimately, I was stuck
I was "on track" to be promoted to Staff Software Engineer for over 2 years, it's not about the time, it's about
continuity, there were times when my manager was going to put me up for promotion and then a reorg happened and I
was moved to a different team, hence, another manager and I was back to square 0 because no one keeps written
track of the team's performance, in a period of 10 months I had 6 different managers and I was "on track", but
it never happened. Even though I was already participating in org-wide initiatives and a lot of the work being
done on microservices and architecture was based on some of my RFCs.

I didn't feel I was growing anymore, I wasn't learning anything new other than a new programming language but on
the implementation side, it was more of the same, a thin wrapper around a data storage (Elasticsearch in this case)
and a REST API with some Kafka consumers and producers.

## Then... performance review was coming
And my manager was quite blunt, "I don't think we can put you up for promotion this cycle", of course, he just
started managing the team, he didn't have any data from my previous managers and when I asked when could we
think of pushing me up for promotion he said "next to next cycle", so, two years and a half from that time.

I was highly demotivated, I showed up for work every day and gave my best anyways but, I didn't think I could
sustain that in time, the work I was doing didn't make me feel accomplished anymore, so... I decided to leave
and join another company, but this is another story.

## Conclusion
Well... Senior leadership **loves** to say the company is *like a family*, I don't buy that anymore, people
don't get laid-off from a family, I see it more like a community, people comes and goes as they please and,
sometimes they're kicked out for some reasons. Seeing things like this changed the way I see work, work is
just that, work, I can love a lot what I do but... I wouldn't get paid to do it if it was all fun all the
time. My colleagues are that, colleagues, some might become friends, but not family, family is waiting for
me at home everyday and will be with me no matter if we're doing well or not-so-well, we figure it out
together without letting anyone go. Family is why I work hard (and smart) so that we can have a good life.

