---
title: "Optimizing a LIKE query"
date: 2023-04-19
tags: 
- "databases"
- "optimizations"
- "software engineering"
featured_image: https://dl.dropboxusercontent.com/s/9b18p5kxfpdm65u/index.jpeg
type: post
---


Premature optimization is quite often the root of all evil, by performing optimizations for 
problems we don't have yet we are doomed to end up with an over-engineered system difficult
to reason about and build on top of.

I'm not saying this because of database optimizations, although, quite often is a bad idea to
add a lot of indices from the get-go, there are some common optimizations which are considered
good practices, for example, primary keys, adding indices on foreign keys and... that's about it.
I appreciate when a database is clean and doesn't have too many indices when the application is
just giving its first steps and doesn't have too much load, which is the case of this one.

# A bit of context
I'm working with a friend on this project called [flexipark](https://flexipark.io) it's a long story,
I've always been involved somehow in the project, just not actively, only as some sort of *advisor*,
just now I joined as an official side-project, we're all working full time while also spending some
time growing this product, I'll write about this later.

This is a Laravel Lumen app (PHP) that runs on Heroku and uses Postgres as database

# The query
Relational databases are limited when it comes to comparing partial texts, for example checking for a prefix
or a suffix or comparing two strings, it gets slow under load, specially because it compares character to character.
This query specifically had a `like SOMETHING_%` condition on the `where` clause, while it's not as bad as
a suffix check (`like %_SOMETHING`), it can also get slow.

`select * from logs where user_id = $userId and pool_id = $poolId and event_name::text() like 'VEHICLE_%' order by created_at desc`

This query, according to newrelic, was taking up to 6s on p99, which is not acceptable.

# The optimization
Since I'm also working full time, I wanted a quick solution to buy some time until I can refactor the data model
into something easier to reason about and closer to the optimal, I thought of denormalizing the `event_name` column
and extract an `event_type` and use it to filter, but it would require deploying the app and potentially adjusting some
unit tests. I had it as a second option if the index didn't give good-enough results.

I ran the query with `explain analyze` and got the following output:

```
Limit  (cost=61307.30..61307.30 rows=1 width=98) (actual time=432.498..432.560 rows=1 loops=1)
   ->  Sort  (cost=61307.30..61307.30 rows=1 width=98) (actual time=432.495..432.554 rows=1 loops=1)
         Sort Key: created_at DESC
         Sort Method: top-N heapsort  Memory: 25kB
         ->  Gather  (cost=1000.00..61307.29 rows=1 width=98) (actual time=0.299..432.346 rows=159 loops=1)
               Workers Planned: 2
               Workers Launched: 2
               ->  Parallel Seq Scan on logs  (cost=0.00..60307.19 rows=1 width=98) (actual time=0.736..424.321 rows=53 loops=3)
                     Filter: (((event_name)::text ~~ 'VEHICLE_%'::text) AND (user_id = 132) AND (pool_id = 26))
                     Rows Removed by Filter: 821012
 Planning Time: 0.127 ms
 Execution Time: 432.592 ms
```

Note that it plans a **Parallel Seq Scan** on logs, this means it will look through all the records

I simply added a BTREE index on the columns used on the `where` clause, respecting the same order they appear on:

`create index log_idx on logs (user_id, pool_id, event_name, created_at desc);`


# The result
After applying the index, I had to check whether it was being used or not, so I ran the same query with `explain analyze` to
see the improvement, if any:

```
 Limit  (cost=8.59..8.59 rows=1 width=98) (actual time=0.617..0.622 rows=1 loops=1)
   ->  Sort  (cost=8.59..8.59 rows=1 width=98) (actual time=0.615..0.617 rows=1 loops=1)
         Sort Key: created_at DESC
         Sort Method: top-N heapsort  Memory: 25kB
         ->  Index Scan using logs_idx on logs  (cost=0.56..8.58 rows=1 width=98) (actual time=0.271..0.473 rows=159 loops=1)
               Index Cond: ((user_id = 132) AND (pool_id = 26))
               Filter: ((event_name)::text ~~ 'VEHICLE_%'::text)
               Rows Removed by Filter: 212
 Planning Time: 0.181 ms
 Execution Time: 0.647 ms
```

As you can see, it did an `Index Scan` using `logs_ids` which is the index I created and the improvement on the execution time
is massive, it comes at the cost of a slightly higher planning time, but the difference is negligible.

# Conclusion
Before jumping in and refactoring, see what other easier optimizations can be done to solve the pain.
