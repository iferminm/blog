---
title: "PyCon: Challenge Accepted"
Author: Israel Fermín Montilla
date: 2017-08-01
tags: 
- "personal"
- "speaking"
- "pycon"
type: post
---

Everyone who has known me long enough, knows that I'm a huge fan of Python programming language,
for several reasons, ease of use, ease of learning, the huge amount of libraries, frameworks and
packages in general available in PyPI, it's not perfect but it has become my tool of choice for
most of my problem solving writing software and career choice, I've been working writing python
code ever since my internship in 2011 and even organized the first and, so far, only PyCon edition
in my home country (Venezuela)

Ever since I organized PyConVE 2012, I was a bit disappointed I had so much stuff to do and handle
for the conference that I didn't have time to speak myself, give a talk or a workshop at the conference
I organized, so, it was on my checklist to speak at a PyCon as a personal goal and kind of a dream,
that's the biggest Python event a local community organizes in their country so it's quite a big deal
for Pythonistas.

This year, I applied to speak at several Pycons, first I applies to PyCon Australia, I got denied but
they provided a great feedback on how to properly write talk proposals to PyCon (thanks a lot guys!).
So, I decided to apply to PyCon Poland, PyCon Spain and PyCon South Africa.

I'm honored and pleased that my talk **django in the real world** was accepted in PyCon Poland and also
in PyCon Spain. I'm still waiting for news from South Africa. I will for sure share my experience in both
conferences, I'm so excited and thankful with my employer, dubizzle, for sponsoring my trip to the conference,
with the organizers for this huge opportunity and specially with my wife because she won't be able to go
with me and she's spending almost one week alone with our baby.
