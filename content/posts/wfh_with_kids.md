---
title: "Working from home with kids"
Author: Israel Fermín Montilla
date: 2020-04-20
tags: 
- "time management"
- "remote"
- "parenting"
featured_image: https://dl.dropboxusercontent.com/s/w6eju98dul5fsqn/people-child-children-computer.jpg
Thumbnail: https://dl.dropboxusercontent.com/s/w6eju98dul5fsqn/people-child-children-computer.jpg
type: post
---

I've been hearing and reading about [COVID-19](https://en.wikipedia.org/wiki/Coronavirus_disease_2019) since January 2020, this is the event that
changed many people lives, all of a sudden, companies were closed, people were requested to
stay at home, many businesses broke and many more are about to. I think one of the most impacted
roles are the ones that require people to be on-site in order to do their job, for instance, hospitality
is taking a huge hit because of the lack of business and hospitality employees are taking a 
huge risk by going to work too.

Tech people such as Software Engineers, Product Managers and everyone who works in the tech industry
building software products running in the cloud are very lucky because all we need is an internet
connection and a laptop, in some cases a VPN setup to access the company resources and that's it, we
can work from anywhere in the planet, in many cases, people are very used to work remotely, nowadays
technology allows you to keep communicated, there's email, cellphones, chat, video-conferencing tools
to keep everyone in touch and, again, all you need is a decent laptop (usually company-provided) and
an internet connection.

# The current situation
When [COVID-19](https://en.wikipedia.org/wiki/Coronavirus_disease_2019) cases started to spike 
[worldwide](https://www.worldometers.info/coronavirus/) many governments started asking people to stay
home to keep people from getting infected, then, it was not an optional thing, it became mandatory in
many countries, here in the UAE, specifically in the Emirate of Dubai, people are not allowed to go
out, only for essential reasons or emergencies. Buying food, medicines or a medical appointment are
valid reasons to go out and a permit needs to be obtained from Dubai Police, this is done online
and it's usually approved, takes only 15min. 

The issue is, almost everyone in the tech industry is working from home, schools are closed
so, kids are also required to stay home. Usually, when you work from home, kids are at school
so you can just be at home and get things done without any distraction if you're disciplined enough.
But, in this case, people might have kids asking them to play with them, demanding attention, siblings
fighting over a toy and, a worried parent, trying to get things done, most companies have understood
that and organized online activities with a focus on kids, Careem is one of these and I'm very proud
and happy to work for a company like this which is willing to, not only give me the flexibility to
care for my daughter when I need to, but also help me entertain her with online activities carried out
voluntarily by my colleagues.

# My personal story
I've worked remotely before, I'm very used to it, for over a year at Careem, even though I was on-site 
at Basecamp here in Dubai, I was the only team member here, the rest of my team was located in Karachi,
Pakistan, so, online communication was the only way to stay in touch, back home in Caracas, I used to work from
home as well at times, but in both cases, I didn't have a 3yo at home wandering around and taking my attention
away from work. 

I love my daughter, I love sitting and playing with her, showing her stuff and her look when
she draws something and comes very excited to show me and explain the drawing for me, but I also have to
work, my boss and my team understand the challenges of working from home with kids and are very flexible,
but, my daughter, and most 3yo kids, have a hard time understanding that daddy (or mommy) needs to 
sit for a long time in front of their computer and do things, so, we need to look for other strategies

# What has worked for me
My daughter is 3 years old, this means she has a lot of energy and wants to play all the time. I've been reading
a great book on parenting called [How to talk so little kids listen](https://www.goodreads.com/book/show/29430725-how-to-talk-so-little-kids-will-listen). It more like "How to not be a jerk to your kids
when they don't want to listen", it gives few strategies to make them be more collaborative without the punishment/reward
technique which backfires after some time and, sometimes, it just doesn't work.

## I want to work with you daddy!
Yes, she actually says that, I can't get her in front of the computer and have her write code, but what I can do
is give her few assignments or fun stuff to do while I work, I ask her to paint something and give her
some brushes and markers, or a coloring book and ask her to finish few pages or draw some letters or numbers, 
and that I'll come back and review her work when she's done. My mom is a pre-school teacher and she sent me
a lot of activity sheets for her (all in Spanish so that she also learns the language).

## I don't want to paint
Well... kids are people too, if someone imposes something to you, chances are you won't like to do it, just like grown
ups, they like to be in control of what they do, so, I give her options "oh, ok, you don't want to paint, then you can
do some playdoh figures or play a game in the iPad" (a carefully chosen educational game), that usually helps.

## I don't want to do anything, I want to play with you
Ok, this has been a tough one, but what I've found was that putting her in charge again works. I've been using the
[pomodoro technique](https://en.wikipedia.org/wiki/Pomodoro_Technique) to manage work time and break time, so, I fully focus for 1hr and then I take a 15 to 20 minutes
break. The way this works is, I tell my daughter to remind me to take breaks, I give her a timer and I tell her
"please, come pick me up when the alarm rings" and then, while playing I tell her "please, remind me to go back to work
when the alarm rings", this has worked so well we've started using it for timing other things 

## When she just doesn't want to collaborate
It happens from time to time, specially when she's sleepy and in a very cranky mood, what I try to do in these cases
is go into problem solving mode (if she's not cranky) I tell her "ok, you want to play but daddy needs to work, what
can we do?" sometimes she says "no, no, no work!" when this happens and if I can't manage to convince her, I just
take a break if I don't have any meetings. Sometimes, when it works, she says "ok, ok, let's put the alarm" or
"I'll cook you something in my kitchen while you work", and she brings me a delicious plastic omelet. 

## I keep my word
If I tell her, "we will do X" we do it, kids are kids but they're not stupid, they remember things and they will
hold you accountable, so, if it's in my control, I hold my promises, if not (because the place I promised to take
her is closed or the item I was ordering for her is not available) I resort to something else like giving her a
handwritten ticket valid for the same thing the next day, she's usually happy about that and reminds me the next
day, ticket in hand.

## We eat together
Yes, breakfast, lunch and dinner, we share and we spend those eating breaks together, having fun while cooking
(or reheating leftovers) and then making the food disappear

# Things to remember
* I've been reading everywhere "This pandemic is not a productivity contest", while this is true, it is also true
that we need to do our best to be as productive as we can, we have other colleagues relying on the work we do and,
more importantly, people relying on the services or apps we build and maintain.

* Remember that kids are kids, you were a kid some time ago, so, try not to explode when things are not going the way you'd like
the best thing to do is to have a deep breath and adapt

* Remember that you're not actually working from home, we all are in a pandemic, locked at home and trying to get
work done. Don't push it too hard, you can always catch up after hours, over night or by waking up earlier than everyone
and working while everyone is asleep

* The more you apply certain techniques, the more they will tend to work in future, so be patient and persistent
