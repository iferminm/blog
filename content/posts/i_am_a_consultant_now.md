---
title: "I am a consultant now"
date: 2023-04-12
tags: 
- "career"
- "personal"
featured_image: https://dl.dropboxusercontent.com/s/unswvt0e3jbv7vq/header.jpeg
type: post
---

I made a career move, I went from a Software Engineering role to a more of a Consulting type of role.
I started in this new role on Dec 5th 2022, I explained the reasons why I left on a previous post, I
want to be more positive and write about why I joined BCG and why I'm enjoying it.

# Why BCG?
I never liked consultancy companies, my first couple of jobs were with consultancy companies and
I think I never worked more than in those first jobs. Whenever a client didn't pay, I was asked to
work on something else, when the client paid again I had 2 projects, I remember working on 8
projects at a time, and it was a lot of coding. However, that doesn't seem to be a problem
in bigger firms like BCG, Accenture and McKinsey.

Even though I had a sour memory from my times working for a consultancy company, I've always liked
two big firms: Thoughtworks and BCG

## Thoughtworks
I think Martin Fowler and the fact that Sam Newman worked there is reason enough to love that company,
a lot of their engineers are published authors and work have came up with very interesting concepts. Being
part of such a talented and brilliant group of people must be amazing. Plus I love their Tech Radar

## BCG
On the other hand there has been a firm which has consistently bet on sustainability and is highly regarded
for their work ethics and that company is The Boston Consulting Group (BCG), this is something I've always
liked about BCG, everything I've read or heard about the firm has always been good.

So, when I was looking for a new job, I was approached by a recruiter working for BCG about a role on BCG-DV,
when I saw it was actually part of BCG I immediately applied. DV stands for Digital Ventures and it's BCG's
venture building arm. I didn't know BCG had an office in Dubai, this was quite an amazing surprise.

# The role
My role is as a Senior Engineer and it's supposed to be a hands-on engineering role, I'm supposed to write
code among other things, in reality I only wrote a bit of code when I was shadowing a venture, the rest of my
time has been building proof of concepts for proposals and writing Power Point slides about engineering practices
and recommendations, which has been also an interesting shift from gathering requirements and implementing to
now thinking given certain characteristics on an organization or team, what engineering practices could fit them or,
given a business domain, how can we make it better using cutting-edge technology.

On a lot of those proof of concepts I had the opportunity to work with technology I wouldn't touch if I was working
on another place, I've build A LOT of small programs using OpenAI's GPT models and interfacing with OpenAI's API,
this has been super cool and gave me a different perspective on the possibilities of Generative AI and what's
possible and what's not yet possible using this technology.

A huge part of my new job is designing systems, getting the requirements from the core consultant and
designing a system that would satisfy those requirements, I haven't worked on implementation yet, but
thinking about systems is something I enjoy a lot and having a strong software engineering background
has been a key factor to be successful in this part of the job. This is all part of the work when I'm
not staffed in any project, supporting in proposals by doing slides about engineering practices, principles
and systems design and building Proof of Concepts with new technologies to assess the feasibility of
what we are proposing.

The whole dynamics are completely different to what I was used to. At all my previous roles, I knew exactly
what to do, I was working on a single project all the time and there was a constant stream of requirements
to detail, estimate, plan and implement, the context was always the same and the technology was always also
the same. Here, one day I'm working on a Proof of Concept for tourism app with OpenAI's GPT model, the next week
I could be designing an engineering team to tackle a microservices decomposition and the week after I might
be working on designing a CICD pipeline to support an agile transformation for a bank, I'm enjoying the
fresh air of dealing with problems I wouldn't otherwise encounter working at a normal tech company and
the learnings of other parts of the business I wouldn't otherwise see working as an Engineer at a normal
tech company.

