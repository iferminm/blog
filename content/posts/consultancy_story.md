---
title: "A Consultancy Story"
description: "Sometimes cheaper is expensive"
Author: Israel Fermín Montilla
date: 2020-06-24
tags: 
- "business"
- "consultancy"
- "career"
featured_image: https://dl.dropboxusercontent.com/s/4sjucrotl4y2ang/header.png
Thumbnail: https://dl.dropboxusercontent.com/s/4sjucrotl4y2ang/header.png
type: post
---

About 8 months ago I was contacted by someone who needed a software project,
the requirements were clear and simple enough for the most part, but it had a huge 
field operations component and it required the software to provide real time metrics 
to the operations teams about what was happening on the ground. Not to mention a very 
complicated workflow for some edge cases.

I went through the documentation they sent to understand what was required, but looking 
at the scope and the timelines they managed, I urged them to start as soon as possible in
order to meet the deadline gracefully and without cutting too many edges.

They were hiring me as a Consultant Architect and Backend Engineer and they were hiring
another freelancer as a Frontend Engineer who was going to only build the client side
in accordance to the guidelines and specs given by me, so, I sent them my quotation with
a description of all the services I was going to be providing as well as the conditions
we both had to comply in order to ensure work gets done seamlessly and on time. As expected
there was some pushback, so, I reduced a little bit, but then, they said they were willing to pay
less than a half of what I was charging them, and this was their budget for the full project,
not just for the backend. I don't know how much the Frontend Engineer was charging them but
this was a deal breaker for me.

To make the long story short, I told them I couldn't reduce further for them, so they outsourced
their project with an agency in South West Asia which delivered their project below their budget
and within the timesines they needed and I took another project. Apparently the quality was not 
great because last week they emailed me asking me if I was still interested in working with them.
Of course, I already had another plus my job and no time to get another one, potentially fixing
someone else's mess, so, I just told them that I was engaged with another client on a long term
project so I wouldn't have the time to get onboard with them.

# WYPIWYG
Basically, when it comes to building software and tech, *What You Pay Is What You Get* (WYPIWYG).
when you hire freelance engineers, you're not hiring a pair of hands to write the code for you,
you are hiring a brain to design, architect, plan and implement a solution that not only works,
but also works efficiently and scales. All of this comes with knowledge and experience after buiding,
and maintaining a number of projects and using different stacks for that, seeing what works better
and keeping those practices but also discarding the ones that were disastrous. 

I understand everyone has a budget, but the fact that they were coming back means that they could
afford it before, it was just that they were seeing no value in it, after all, *how hard can it be
to build few forms and some pages*. But in the end, it's not about building few forms and some
pages with graphs, every application out there in the web is an Engineering project, no matter
how small or big it is, there is Engineering behind it.

# It's not easy
Softare Engineering and Tech in general is among what is called the Knowledge industry, your tools,
besides a keyboard and a computer with decent specs, are your brain, which is your knowledge and
practical experience and the ability you have to reason about problems and how to solve them in
efficient and, sometimes, creative ways, so, in the Knowledge industry, everyone wants to be the
smartest guy in the room and tries to make things look easier than they are, minimizing the work
itself or only showing the 3 lines of code that it took to solve a problem, hiding all the thought
process, failed attempts and mistakes under the rug, as I posted once, [there's so much more to
software engineering than just writing code](http://iffm.me/software-engineering.html)

If we minimize our work, trying to make it look easier, people won't give it the value it deserves
and go for cheaper options which could not always be the best choice when it comes to quality.

# Good, pretty and cheap. Choose two
In Venezuela, we have a saying about things, whenever we are buying something we look for something that
has the three of them, it has to be good, because no one likes poor quality stuff, it has to be pretty,
because no one likes ugly things and it has to be cheap, because no one likes to spend a lot of money
on things. But usually, nothing is good, pretty and cheap at the same time, so, everything comes with a
tradeoff and you need to choose your balance.

Whatever is good and pretty, is not cheap. Whatever is good and cheap, is not pretty and whatever is
pretty and cheap is not good and will break sooner than you think and you'll have to get another one
or call someone in to fix it, maybe the expensive freelance consultant you spoke to some time ago.

Have you ever had a similar experience with a potential client? how did you handle it?
