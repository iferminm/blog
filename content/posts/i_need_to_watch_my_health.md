---
title: "I need to watch my health"
date: 2023-04-19
tags: 
- "health"
- "personal"
featured_image: https://dl.dropboxusercontent.com/s/3e17umcbdm50i8p/need-to-watch.jpeg
series: ['My health']
type: post
---


One of the things about traveling a lot for work is that I have to adapt to different local
realities, the concrete example I'm talking about is that in this project, I have to
visit Riyadh, Saudi Arabia, every week. That's fine, I like Saudi Arabia for very silly
reasons I guess I'll write about later, the issue was that I had to visit during the Holy
Month of Ramadan.

For those who doesn't know, the Holy Month of Ramadan is when God revealed the Holy Quran
to Prophet Muhammad (PBUH), Muslims all over the world fast during the whole month, in the
UAE is a very happy season, there are lights everywhere, it feels like Christmas to me as
a Catholic. In Saudi Arabia, it also feels like that but, since everyone is supposed to be
fasting, most restaurants are not open or open at limited capacity, so, that week I was there
the only open place nearby the client site was... McDonald's, and I was lovin' it!.

So, yeah, I was having McDonald's every day, it was the only restaurant open nearby and on the
3rd day, I reached the hotel after work as usual, took a shower and when I was getting dressed,
I felt my head hot as hell, like when you go to the beach and spend too much time under the sun,
my ears felt like they were in flames and I started having a very strong headache, I called reception
and asked if there was a medical service at the hotel because I wasn't feeling well, they sent the
hotel nurse and took my temperature and it was OK, but my blood pressure was skyrocketing, it was
too high. They kept me under observation for a while to see if it came down, otherwise they would have
had to rush me to the hospital.

This was the first time something like this happened to me, back in Dubai, I visited a Cardiologist
and she explained me it was normal, after having McDonald's so often, because of the salt, the blood
pressure will go up. She asked me some other questions:

* *Do you smoke?:* - Nope
* *Do you drink?:* - Well, socially, maybe 2 to 4 glasses of wine... not every day, not even every week
* *Do you exercise?:* - Nope
* *Do you eat healthy?:* - At home, yes, when traveling, I try to, but sometimes it's difficult

As of now, I have a blood pressure monitor attached to me, it measures my blood pressure every 30min for a day,
then I'll have to go back to my cardiologist and see the results, I have some other blood tests scheduled. Since
I have diabetes history in my family, I also visited  an endocrinologist and, since I am on the spectrum I'm visiting
a neurologist too.

I have a lot of blood tests to do... I'll update soon on another post... but, for now, please take care and
don't eat junk food every day.
