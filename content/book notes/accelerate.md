---
title: "Accelerate"
date: 2023-04-20
tags: 
- "book notes"
- "devops"
featured_image: https://dl.dropboxusercontent.com/s/1co518dzlq6fk78/header.jpeg
type: post
draft: true
---


# About

Accelerate is a book by Nicole Forsgreen, Jez Humble and Gene Kim about the science behind
Lean Software Development and DevOps, they took a scientific approach to measure the impact of
both practices in different software teams and explain everything with great detail and a very
didactic approach.

# Notes

## About maturity
* Focus on building *Capability*, not reaching *Maturity*
  * *Maturity models* assume that *Level 1* and *Level 2* look the same across all organizations, which is not true
    * When consultants sell maturity models it's a red flag as it is the ultimate scam for tech organizations
  * *Maturity models* assume the company has to *reach* a certain level to call itself *mature* and done with the journey
  * *Maturity models* follow a linear formula every time, assuming it will work for every organization
  * *Capability models* focus on the outcome and how the capabilities are driving improvements on those outcomes
  * *Capability models* help the organization continuously improve and make progress rather than *arrive* at a *mature* state
* The most innovative and productive organizations don't consider themselves *mature* they rather continuously assess and improve

## About measuring performance
* *Velocity* and *productivity* are bad metrics to use for measuring software teams performance
  * When we request teams to finish faster they will take shortcuts to make sure they will hit the target, resulting
    on buggy code reaching production, an increased number of incidents and a harder to maintain system.
  * When we request teams to produce certain number of *features* or certain number of *lines of code* they will
    also develop practices to ensure they will hit the target. Features can be released half baked or untested just
    for the sake of adding up the number of features and developers can start writing less concise code in order
    to achieve the number of lines of code they have as a target. Again, resulting on an unstable and harder to maintain
    system
  * Software teams can also trick their sprints to make sure they burn the right number of *points* and achieve the
    desired velocity
* *Utilization* is another common metric for measuring teams performance which is also bad, when utilization reaches
  a certain level there's no room to absorb unplanned work such as fixes or enhancements. Queue theory math says that
  when utilization reaches 100%, lead times tend to infinite, which means the higher the utilization, the more difficult
  it gets to get anything done
* Clustering analysis was performed among the surveyed organizations in order to group similar answers and compare their
  performance, the results of this analysis can be found on pages 19-22 (buy the book don't be cheap!)

### Good metrics for measuring performance
* **Lead time:** how long does it take to finish work. Time from commit to deployment
* **Deployment frequency:** as a proxy for batch size. Smaller batches of work contain less errors but this can't be measured,
  instead we measure deployment frequency, more frequent deployments means smaller batches being deployed
* **MTTR:** Mean Time To Recover, when there's an outage or an incident, how log does it take to bring the systems back to
  normal operations
* **Change fail percentage:** how many incidents are produced as a result of a deployment

### Low vs High performers
Research showed that high performing organization focused on continuous improvement and automation of their
delivery process via DevOps out-performed the rest by several orders of magnitude they:
* Deployed code 46 times more frequently
* Their lead time from commit to production was 440 times faster
* In the event of an incident they recovered 170 times faster
* Only 1 out of five deployments gave failures in average

## On the impact of delivery performance
* Hight performing organizations were twice as likely to achieve or exceed their goals
* High performing development process is correlated to high organizational performance 
