Title: About me...
Date: 2017-03-26
Author: Israel Fermín Montilla
Tags: about, personal

Hello there!

I'm Israel, and I'm not as bad as they say in the news. I'm a father, a husband, a son, a brother, a friend and also a software engineer who likes to
spend most of his time with his family, reading or experimenting with programming tools and techniques or technology in general. I'm originally from
Venezuela and in 2014 I married Giselle (my wife, obviously) and moved to Dubai with her and in 2016 we got a wonderful gift, our daughter Antonietta.

I'm interested in music (writing and composing as well as listening to good music), and computer science specially programming languages (how to
implement them and how to use them effectively) and machine learning as well and software engineering, specially software architecture, tuning,
performance and testing / verification. You can check my now page if you want a more specific overview of what I'm doing right now.

I love to read, and I'm always open to get good books recommendation, you can add me on
[goodreads](http://www.goodreads.com/user/show/43392734-israel-ferm-n-montilla) follow me on [twitter](http://twitter.com/iferminm) where I sometimes
say something interesting or add me on [LinkedIn](https://www.linkedin.com/in/israel-ferm%C3%ADn-montilla-7393721a/) if you wanna get in touch. Or,
[buy me a coffee](https://www.buymeacoffee.com/iferminm) if you think anything I write is interesting.

You can also send me an <a href="mailto:iferminm@protonmail.com">email</a> I'm always open to meet new people and hear about nice opportunities even though I'm not
actively looking for a career change right now.

You can contact me to:

* Say Hi!
* Go out for a beer or coffee
* Mentor you or your startup
* Advise your company
* Consulting
* Speak on your event

Thank you for taking the time to read this page, if you got this far, <a href="mailto:iferminm@protonmail.com">let's talk</a>
