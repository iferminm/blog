Title: Talks
Date: 2018-03-27
Author: Israel Fermín Montilla
Tags: talks, conferences, speaking 


One of the things I like to do is to share what I know with other people,
for that, I like to do either *1:1 mentorship* when time allows, but
most commonly I like to speak at conferences, meetups or any kind of
event where my knowledge is relevant and people are interested in what
I have to say.

I've spoken on several events in both English and Spanish back home in
Venezuela as well as in the UAE, Poland and Pakistan and I've been invited
to other conferences in Spain and South Africa.

I have a set of talks already prepared I've given at different conferences
and I'm preparing many more. If you're looking for speakers for your event
and anything here calls your attention, [drop me a line](mailto:iferminm@protonmail.com)
so we can talk and coordinate how I can speak at your event. If you think
there's something you'd like me to talk about and it's not here yet,
[let's talk](mailto:iferminm@protonmail.com)

## Talks

### English

#### Software Engineering: more than writing code
We, as software engineers, are so focused on getting better and better at writing code
we forget about other aspects of the software engineering role, we do (and have to do) many
other things in and around software than we realize, in this talk I go through the difference
between a programmer, a software developer and a software engineer, stressing on few soft skills
which are needed to be successful in this career.

**Duration:** 45 to 60 minutes

**Given at:** Private event


#### Django in the real world
There are a lot of projects and systems out there built on top of django. From complex 
high profile systems and intranets to simple websites and personal blogs. Truth is, 
in most of the cases the out of the box django setup will suffice, but in some cases, 
if you're lucky enough, the team will need to make the project scale to serve 
thousands of requests per minute.

There's no recipe to scale a project, that's true, but there are some strategies and 
techniques you can apply to your django project to improve your system's scalability 
and take more out of your available resources. All of this requires to have some clear 
Software Engineering concepts, know your stack and take advantage of other tools to measure 
your project's performance to know what and where to optimize.

In this talk, we will study some of these strategies, using some problems we've faced at 
different companies I've worked for as case of study. We will go through the issues we faced 
on some of our services and how we solved them with real life examples.

**Duration:** 30 to 40 minutes

**Given at:** [PyConPL 2017](https://pl.pycon.org/2017/index_en.html) (Ossa, Poland), PyConPK 2017 (Lahore, Pakistan) (via Zoom), [PyConBalkan 2018](https://www.youtube.com/watch?v=k520PnB-xHo)
(Belgrade, Serbia)

#### Code review the right way
Sometimes, as Software Engineers, we are so hooked into the process and overwhelmed by deadlines and so focused on writing code that we
forget the quality of that code is not only measured on a green build, it's also measured on how much other developers can understand it.

Nowadays, developer's time is more expensive than machine's time, so, the amount of time an engineer takes to understand a piece of code
is more expensive than the time a server takes to execute it, in the end, you want your teams to move faster so your product
evolves and ships faster, that's why it is important to follow universal standards like PEP8 and set your own, but you can't stop
there, you need to enforce them somehow.

In this talk we go through the process of automating repetitive and obvious things to enforce standards and good practices
through all the codebase in a distributed team, by using a set of tools such as linters, static analysis and processes like
Continuous Integration, as well as examples and good practices for *manual* code review, how to do it properly and in an
efficient manner and how to receive it the right way

**Duration:** 30 to 40 minutes

#### Querysets can do that?
The ORM is one of the most underused parts of django and yet, one of the most powerful tools
this full-stack framework offers out of the box, it is there to make of life easier and help up
retrieve all the information we need from the database when we need it and in the most efficient
possible way, we only need to know the right keywords.

**Duration:** 30 to 40 minutes

**Given at:** [PyCon Balkan 2018](https://www.youtube.com/watch?v=3WEUsO9wSBY) (Belgrade, Serbia)


### Español

#### Introducción a las artes marciales con Python
Es una charla de *advocacy* en el uso de Python como lenguaje para principiantes
con conocimientos de programación o que quieren iniciarse en ello y ya han realizado
algún tipo de investigación previa y aún no deciden con cuál lenguaje empezar. 

En esta charla repasamos con ejemplos ilustrados algunos conceptos básicos y diferencias
entre Python y otros lenguajes populares.

**Duración:** 20 a 30 minutos

#### Testing: la etapa olvidada
En todo proyecto de software los requerimientos deben ser implementados *para ayer*, esto
no nos deja mucho tiempo para pensar la solución más óptima para el problema que tenemos
a mano ni mucho menos para escribir pruebas automatizadas que verifiquen que nuestra solución,
al menos, funciona como se espera. *ERROR!*

En esta charla repasamos los beneficios del desarrollo guiado por pruebas (TDD por sus siglas
en inglés) así como la importancia de escribir pruebas a distintos niveles para asegurar que
nuestro software tiene el funcionamiento deseado y que, más allá de eso, cuando estemos *mejorando*
nuestra solución estemos seguros de que si hacemos algo que rompe el funcionamiento normal, una
prueba fallará y nos dirá exactamente qué estamos haciendo mal. Además, veremos los argumentos
más frecuentes en contra de las pruebas unitarias y cómo responder a ellos, así como también distintas
herramientas basadas en Python para solventar varios de los problemas comunes a la hora de escribir
pruebas. Al final, se muestra un ejemplo usando varias de las herramientas mencionadas.

**Duración:** 30 a 40 minutos

**Dada en:** [PyDay Aragua](https://www.youtube.com/watch?v=qtgBpA8-Als&list=PLe_-7MJlIjxRm-Z8WhBmS2pII4rtBGPQh&index=2&t=0s) (Maracay, Venezuela)

#### Comunidad: orden dentro del caos, caos dentro del orden
La unidad básica de organización en el mundo del Software Libre es la Comunidad
de usuarios, las hay en todos los países y es común que organicen eventos y se comuniquen a través
de una lista de correos, canal de IRC, Slack, Whatsapp, Telegram o cualquier medio que resulte
conveniente a los miembros, es común que haya algo de inactividad y que siempre haya gente queriendo
*hacer algo* y esperando a que *alguien* les apruebe.

En esta charla expongo mi experiencia en [PyVE](http://pyve.github.io) organizando varios eventos 
y ayudando a otros a organizarlos en otros estados de Venezuela, cómo mantener la motivación y la moral
alta en una comunidad donde hay muy pocos usuarios acivos y cómo generar *moméntum* para hacer cosas
más grandes.

**Duración:** 20 a 30 minutos

