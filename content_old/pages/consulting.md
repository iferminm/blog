Title: Consulting
Date: 2020-04-12
Author: Israel Fermín Montilla
Tags: consulting


I also do freelance consulting onsite or online, depending on where you are located.

I might not might not get involved in writing code for you depending on the project itself,
and the agreement we have, but I'm always willing to review what your team has done for you.

The fields I cover are:

* DevOps, automation and CICD
* Tech leadership
* Software, systems and cloud architecture and design
* Service oriented architecture and migration to microservices
* Performance optimization
* Requirements analysis

I also enjoy advising non-technical startup founders scoping and building their MVP.

[Give me a shout](mailto:iferminm@protonmail.com) if you'd like us to talk.
