Title: Now
Date: 2017-03-29
Author: Israel Fermín Montilla
Tags: personal, now

    So, what are you working on... now?

Well, I'm mostly focusing on few stuff right now in no particular order.

* Blogging at least twice a month
* Reading about *Distributed Systems*, *Software Architecture*, *Software Engineering* and *Parenting*
* Working on my personal projects:
    * [The Restaurantologist:](https://therestaurantologist.com) a blog to find
    resources about the restaurant business both for restaurant owners and foodies
    who want objective and unbiased restaurant reviews
    * [My Blog:](http://iffm.me) this website, I blog about anything that comes
    to my mind
* Learning new programming languages, I'm currently learning Go and Rust
* Speaking (only online during COVID-19 crisis) at meetups and conferences
* Being part of the backend Tigers team @ [Careem](http://careem.com)


*Last update:* 2020-08-05
