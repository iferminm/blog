Title: Engage!
Author: Israel Fermín Montilla
Date: 2020-03-30
Tags: career, personal
Cover: https://dl.dropboxusercontent.com/s/y7hf2p40crdu19y/careem.jpeg
Thumbnail: https://dl.dropboxusercontent.com/s/y7hf2p40crdu19y/careem.jpeg


I was playing with my daughter the other day, I had work to do but I take breaks
every now and then to pay attention and play or watch some episode of her favorite
cartoons on Netflix as I mentioned [on a previous article](http://iffm.me/working-from-home-with-kids.html). 
So, I was waiting for the alarm to ring and checking the notifications on my phone
if there was an important email or slack message to reply to or a VictorOps ping
about a production incident, all of that while playing with her *Frozen* toys to
rescue *Elsa* from *Prince Hans* who kept her locked in the castle. When the
break finished, it also meant the play time ended, the alarm rang and I said
something like "oh, it's time for daddy to go back to work" while I started 
getting up she interrupted me "but we didn't play!", then I said "of course we
played, we were rescuing *Elsa*" and she threw a killer argument "you were on your
phone". I took a deep breath and realized it was true, truth was, I spent more time
checking on my phone rather than engaging in the game with her.

We live in a very distracted world, in a very connected society, we are always
online and not only that, we are always on top of our phones waiting for the
next notification to come so that we know what was the latest event on someone
else's life or the latest comment on something we posted or, in my case, 
what's the latest instant message on my slack workspace. 

# Always distracted
Seems like we are unable to focus on doing a single thing and geting it done
in one sit, the ability to focus for a large span of time is lost, no matter
what it is, whenever we get a notification or feel the phone vibrating inside 
our pocket, we dump whatever it is we're doing and check what happened, then
we check other apps, then we put the phone away, sometimes we check even if 
it didn't beep!, why? why does a tiny device have such power over ourselves?

# Change!
My daughter's statement made me realize I do that, I have that tendency to always
check my phone, even if I don't have any notification, it made me feel so bad about
myself, I don't want to be dominated by a device, I felt like Pavlov's dog, conditianated
to change my behavior whenever I hear a beep from my phone. So, I decided to change.

## Take action
I started by eliminating all temptation, I muted all notifications on my personal phone
except for whatsapp and telegram, the only reason to keep those is my family, if they
write I want to reply, but I muted all the groups, I'll check them only when I want to,
same for social media apps.

I quied facebook and instagram about a year ago and I don't miss them at all, I'm only
on twitter, reddit and linkedin, all of them muted on my phone and I only check them when
I want to, not when they tell me *hey, you have a message* or *hey, someone liked something*
same goes for gmail, I check it only when I want to, usually on my laptop.

On my corporate phone I have notifications for almost everything, that is basically slack, gmail
and victorops, but I have silent hours for slack so I don't get bothered at all times, specially
over the weekend.

### The most important thing
The most important change is not how to setup your notifications on your phone, is deciding
to change yourself and choosing to engage on the activity you're doing **here and now**, whatever
is in front of you is what's important and should get 100% of the attention, engage!, if I'm playing
with my daughter, I decided to engage 100% and be all into it, if I'm working on a project, I engage
on that and fully focus on it for the time I have or decided to put into it, if I'm talking to someone
I'll be 100% into the conversation, not checking my phone. Some app installed on my
phone is not going to take me away from **here and now**, because it's the only thing we have and the
only thing we live directly, a message from someone hundreds of kilometers away is not more important
than whatever is in front of you, interacting face to face. 

Stop letting the algorithm distract you and decide to engage!
