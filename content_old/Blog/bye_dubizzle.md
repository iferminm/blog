Title: Bye bye dubizzle, hello Careem!
Author: Israel Fermín Montilla
Date: 2018-03-30
Tags: career, personal
Cover: https://dl.dropboxusercontent.com/s/y7hf2p40crdu19y/careem.jpeg
Thumbnail: https://dl.dropboxusercontent.com/s/y7hf2p40crdu19y/careem.jpeg

I've been quiet for the last two months, I was a bit busy switching jobs, yes,
after more than three years I decided to leave *dubizzle*, part of the *OLX*
group and one of the biggest brands here in the United Arab Emirates, for reasons
I won't discuss in this post and some reasons I'll expose here.

My good friend [Gerardo Barcia](http://gerardobarcia.com/blog/) used to quote a lot
a phrase which author I don't know:

> Change is the only constant in our lives

I liked that, I like to think that one needs to come out of your comfort zone from time
to time as the only way to evolve as a professional and as a human being, when change
comes to you constantly, you're done, when you can seek change easily it's also good,
you learn, you innovate, you become better you put that brain to work.

That was one *the thing* I loved about working at *dubizzle*, it was dynamic, I got
to work on interesting projects, I got to challenge myself and it was **GREAT!!**.

I don't know what happened or when, but things became just **good** and then **meh**, everything
slowed down and I didn't feel challenged anymore, in fact, I felt like I was solving the
same problem over and over and over again, I simply stopped having fun, the projects
I was working on didn't motivate me and I wasn't feeling I was having a positive impact
anymore, so, change didn't come to me, so I started looking for change, trying to be
moved to a different department or a different role and finding a lot of resistance.

When you stop enjoying something, you should stop doing it and that was exactly what happened,
I stopped enjoying my job at *dubizzle*, for several reasons but I believe this one was
the most important one, so I decided to move on. Here in the UAE, there are several big
players in the tech scene: *dubizzle* is one of them but not the only one, there's also
*Souq*, *Namshi* which are e-commerce websites, *Fetchr* which is a delivery company and
*Careem* which is a ride-hailing company. 

I interviewed with several but I wanted to do
something different, I was into classifieds for too long and e-commerce I don't think is
that different, so, when the decision came down to *Fetchr* or *Careem*, I started digging
deeper into both companies and there was one talk by one of *Careem* co-founders, Magnus Olsson,
that made me think, well *Fetchr* looks like a great company, also *Careem*, but *Careem*
has this guy with this amazing story and the will to do something meaningful and you can
see that even in the name of the company, *Careem* means *generosity* in Arabic language.
So, here I am, being Careem at *Careem* and helping *improve the people's lives and
build an organization that inspires* one line at a time.


## Recommended:
* [The video](https://www.youtube.com/watch?v=HrSYYmaKmmo) that made me join [Careem](https://careem.com)
* [dubizzle in Glassdoor](https://www.glassdoor.com/Overview/Working-at-Dubizzle-EI_IE670451.11,19.htm) looks like a lot of people are not happy, I hope they course correct before it's too late
