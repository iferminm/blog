Title: Which languages should I learn?
Author: Israel Fermín Montilla
Date: 2020-06-01
Tags: software engineering, career, personal
Cover: https://dl.dropboxusercontent.com/s/u2f9jcmmskrjzdo/header.jpg
Thumbnail: https://dl.dropboxusercontent.com/s/u2f9jcmmskrjzdo/header.jpg


There are millions of articles and blog posts out there trying to
tell you what to learn next or which language or technologies are 
worth your attention, things like *Top 10 programming languages* or
*Learn one of these languages and you'll be done* with some statistics
about language popularity but the truth is, none of those make sense
no one can tell you what to learn and you don't need to study whatever
is popular out there all the time.

Truth is, while popular languages are widely used, also, supply is high,
hence, salaries will tend to be lower, on the other hand, if you learn
the not-so-popular ones, salaries would be higher but you'll end up
most likely having a hard time finding a job.

# Trends come and go
Whatever is popular today, might be considered terrible in the future as well
as what's not so popular today, might be widely used in future, you just have
to see the growth trends in whatever languages or technologies you're into.

Few years back, for example, JavaScript was used only for frontend development
and JQuery was the best thing out there for it, now almost no one uses JQuery
directly, everyone is using react, angular or vue and now you can even find
JavaScript on the backend (not that that's a good thing IMO...).

The point is, we should not chase the top 10 most popular technologies, we should
try to find a balance, otherwise we won't really fully learn nor master anything.

# My view
I think that, the first thing we need to do is, work on what's paying your bills.
Few years back, for example, Python was the one paying my bills and it happened to
be the first language I worked with after graduating, I became passionate about it,
to the point that I organized meetups and conferences back home in Caracas - Venezuela
and after that, I only looked for jobs in Python, it was gaining popularity and growing
a lot, I know, I was lucky, but it also makes a point, I was focused on becoming
better on a single thing which were the technologies I was working with at that point
in time, the full stack was Python, Postgres and OpenERP (now it goes by Odoo).

I think everyone should start by that, just become better and learn more about
the stack you're currently working with. My last 100% Python job was at my
previous company (dubizzle/OLX), when I joined Careem the stack I worked with
changed to Java, Springboot and MySQL mainly, so I started over, learning a new
framework and a not-so-new language to me. Then, I switched teams and the stack
was Python, Jenkins (groovy) and Terraform, again, learning new things. Then, after
almost 2 years, I again switched teams and the stack was Typescript, Express, Node,
and both MySQL and Postgres and after few months I was changed to a different team again,
the stack also changed and now, this team works on PHP and Go for the most part and
MySQL. Whenever your stack at work changes, focus on learning and mastering that.

Once you master whatever is helping you pay your bills, you can use your spare
time to learn whatever you want, in my opinion you need to know at least one
of those popular languages just to make sure if something unexpected happens 
to your current job, you can find a new one quick. In my case, maybe I've been
just lucky, I know Python, Java and JavaScript and those are three of the most
popular languages out there today. But I also know their popularity won't last forever
and that some less popular languages might pay better salaries because supply is
less and the skills are more rare.

Everyone is free to learn whatever they want, but this is how I think about it:

* **Start by what pays your bills:** this is important, you need to be efficient at
your work and the only way to achieve it is by learning how to use your tools
effectively. This will help you do more in less time.

* **One popular language:** well, at least one, if you like more than one, go for it,
why learning the popular ones?, this will open the door for more job opportunities,
you never know what will happen and it's better to be prepared to hunt (or be hunted)
for a new role on a different company.

* **One high level language:** the higher level, the best, I'd go for a language with
a dynamic but strong type system, why? to build things quick. Usually high level
dynamic languages have a huge ecosystem of libraries and frameworks you can leverage
your work on and achieve more writing less code.

* **One lower level language:** you never know when you'll need either more speed or
write something at system level or even have more control over how the program will
manage memory. Truth is, not all use cases are covered by a High Level object oriented
language.

It's not too hard to achieve that, you can mix and match languages in all those buckets, 
for example, in most of the cases what pays your bills will be a popular high level
language, that covers the first three, then you'll only need to learn a lower level
programming language to be all setup.

Once you cover that, you can explore other paradigms and incorporate more patterns
and practices from those paradigms and other languages into your every day programming
style, by learning, understanding and practicing is the only way we become better
software engineers.

In my case, as I know JavaScript/Typescript, Python and Java because of previous jobs,
maybe not all of them at an expert level but at least a general understanding so that
I can tackle most of the day-to-day tasks using them.
I'm focusing on learning Rust, it's a language which is growing and implements interesting
concepts and I've toyed around with Smalltalk and Haskell to see what's there to learn and 
incorporate new practices into my coding. 

What's currently on your learning list? let me know on the comments box.

