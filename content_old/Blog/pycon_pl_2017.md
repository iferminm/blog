Title: PyConPL 2017
Subtitle: my first time as a speaker
Author: Israel Fermín Montilla
Date: 2017-09-01
Tags: personal, speaking, pycon

Two of my personal goals for this year were:

1.- To speak at a local meetup here in Dubai

2.- To speak at a bigger conference, anywhere

Thanks to my wife, who is always supporting me much more than I support her, I could do
both, first one was at [Dubytes](https://www.meetup.com/Dubytes/), also, thanks to my colleague
[Dareen Alhiyari](https://twitter.com/dareenalhiyari?lang=en) who runs that meetup group.

The second one was at [PyCon Poland](https://pl.pycon.org/2017/index_en.html), this year (2017),
my talk [django in the real world](https://speakerdeck.com/iferminm/django-in-the-real-world) was
accepted and my employer, [dubizzle](https://uae.dubizzle.com/), sponsored my trip there.

My experience in Poland was great so far, the conference was at [huge beautiful venue](http://www.hotelossa.pl/en/) 
one hour away from Warsaw, so I arrived one day before so I could walk around Warsaw and see some
historic and touristic places, [the old town is beautiful](https://goo.gl/photos/pyRveSzZ6wAQQwnE6),
and that was pretty much all I could see, didn't have time to do any museum before
going to [Arkadia](https://en.wikipedia.org/wiki/Arkadia_(shopping_mall)) do to some shopping,
for my wife and my daughter.

Public transportation is quite straightforward, the only issue is you have to ask people to write down
things if you ask for directions, everything is in Polish and it's not pronounced the same way as
written, with some time you get it, but I ended up asking people to write it for me the first day.
People were really helpful, every time I struggled with any of the ticket vending machines, someone
jumped in and helped, even if they didn't speak too much English, they did their best to understand
and help.

I finished all my shopping and touristing by 22:30, so I headed to the hotel, which was just in front
of the airport, at the other side of Warsaw, I took the Tram, walked a bit to the main bus station
(I think) and waited for the 175 bus heading to Chopin Airport, at the station I had to use my
Latino defensive skills, some drunk guy tried to steal my wallet, I was carrying a lot of bags, I
was obviously a tourist, but you just don't try to steal something from a South American without at 
least a knife.

I arrived at the Hotel by 23:30, tired, took a bath and went to sleep.

## Getting to the conference
I packed all my stuff, checked out from the hotel after breakfast and headed to the meeting point
at Chopin Airport, conference attendees started to crowd and I started to talk to random people, 
dubizzle asked me to recruit so, I started to try out my skills talking to people, asking what
they do with Python and speaking about what we do and how we do it at dubizzle, some people looked
excited when I talked about employment opportunities, some others not so much, apparently the hot
weather in summer is a deal breaker, the bus arrived, I checked in with the organizers and
boarded, it was a roughly 1hr trip to the venue.

## The Conference
Experiencing a conference as a speaker and as a normal attendee, is almost the same, as a speaker
people asks you much more questions about what you do and about what you will talk about, I spoke
to a lot of people with the intention of recruiting them, I was impressed about how well prepared
most people were, they had master degrees or pursuing PhD studies, academic titles mean nothing nowadays,
but they do tell you that the person takes care and time on improving himself and gaining more knowledge.

It was very well organized, and there was always someone from the organizing team available to help
and to answer questions. People were also impressed about the fact that I went all the way from Dubai
to speak at their conference, I was humbled and honored to be there to be honest.

During the conference I was more into talking to people, I did attend to some talks, but my main
focus was networking and getting people into applying to work at dubizzle or sending me their CV.

## My Talk
I'd say it was great, I got a lot of questions, which is a good sign, if you get no questions it means
that people were either not listening or did not understand a single word.

I'm waiting for them to upload the videos from the talks to watch how I really did and some other talks
I was interested in but couldn't attend.

## Conclusion
I really liked the experience, I would definitely encourage everyone to apply to speak at this kind of
conferences, I'll apply to speak at PyConPL next year for sure and will take my family as well, Warsaw
is a beautiful city, full of history and nice people. Will come back for sure!.

Also, if you're looking for a job and would like to relocate to Dubai, have a look [here](http://blog.dubizzle.com/uae/job-vacancies/)
at the open positions and leave me a comment, we can talk about it.
