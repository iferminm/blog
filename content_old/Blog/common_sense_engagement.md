Title: Work at a company that puts people first
Author: Israel Fermín Montilla
Date: 2021-04-14
Tags: career, personal
Cover: https://dl.dropboxusercontent.com/s/g2hnjo0xsaixsuw/IMG_4173.jpg
Thumbnail: https://dl.dropboxusercontent.com/s/g2hnjo0xsaixsuw/IMG_4173.jpg

The title says it all, easier said than done right? the truth is that it takes
a lot of time for some people (and not so long for some others) to find a place
where they feel welcomed and belonging, it's a quite cool feeling when people
appreciate you and makes an effort in making you feel comfortable at work in
every aspect by always taking into account what's important to you and helping
you with that.

For me, I've been working in the software industry since I was studying, and I
have a lot of stories. Most companies value the business first, and that's
completely valid, if the business is OK, everyone is getting paid and everyone
is happy. But I feel I don't last long in this type of companies, I value
flexibility, every company has its rules and every manager has their values and
also set of rules they use to run their teams, but I believe everything needs to
be evaluated in a case by case basis, not everyone has the same conditions
or priorities in life and this is where this approach fails. Let me share a brief
story of my professional life until now and why I've left some of the companies
I worked for.

## My first job
My first *official* job, and by *official* I mean, for a company, with a contract
and not doing something kinda freelance for a friend, was with an online news
hub, they scrapped news websites and presented all the headlines from different
sources in a single place, people could customize which sources they preferred 
for certain topics and filter based on that. My job there was to build bots to
scrape multiple sources, extract the content and store it in our system. I was
still doing my undergraduate courses so it was a part-time thing.

Everything was going great! salary always on time for about 3 months, then...

By the end of the fourth month, I didn't see the salary being credited to my
account, I didn't pay too much attention, maybe there's a delay with the bank
or they didn't process the payroll on time or whatever, I emailed my manager and
I got an auto-response that he was on vacation, half-way into the month I email
my manager's manager asking what happened...


The answer was *yes, you didn't get paid because you're not working with 
us anymore*, I was a bit shocked because no one told me I was let go, they
conveniently forgot to tell me that little detail, wait a minute, this gets way
better, I highlighted exactly that and the answer was amazing: *well, Israel,
if you see the contract it says any party can end the employment agreement. It
doesn't explicitly mention any notice, technically we didn't do anything wrong*.
Long story short, I had to threat them with going to the labor authority and
report the issue in order to get my last salary... almost 3 months after.

### Lesson learned
There's very bad people owning and managing companies, I know I was undergraduate
but they even used that fact to imply I should be thankful about them paying me
for my work because, and I quote textually, *a lot of students would work for 
free just to get the experience*, well, I wasn't one of those students...

## My internship, great! until it wasn't
My first professional work experience was on my internship, I did it with a
small local OpenSource consultancy firm, I learned A LOT!, the work environment
was great and fun, workmates were cool, smart and the kind of people who knows
what they're doing. The only bad thing was salary, as I was an intern, my salary
was low, but I was OK, at least I was doing things I was into and learning quite
a lot.

When I finished my internship, they offered me a permanent position which I
accepted, they were aware I was still having academic commitments and they were
OK with that. I had to work on my thesis project so, after some time I asked for
some time off to do that, they offered me a room at the office where I could lock
myself up for up to 3 hours a day to fully focus on my thesis, this was awesome but
the only bad thing was it was respected for a bit over a week, after that I started
getting constantly interrupted to work on other things, we went back and forth
with this for a little over 2 months until I decided to quit so I could focus
on my thesis.

### Lesson learned
Sometimes your priorities and the organization's priorities are not aligned, and
this is fine, when this happens the best thing to do is to part ways. Maybe if
I was given that time off to complete my thesis as I initially requests I would
still be working there. The company, the projects and the team were awesome.

## Fast forward to my first job in Dubai
I won't be talking about how I got a job in Dubai and how I got here, [I wrote
about it before](https://iffm.me/new-grads-survival-guide.html), I won't write about how cool it was because [I already did it](https://iffm.me/working-at-dubizzle.html)
but I'll dig a bit deeper on the real reasons behind me leaving that company.
I wrote an [article before about it](https://iffm.me/bye-bye-dubizzle-hello-careem.html), but I wasn't completely honest about why
I actually left because I didn't think it was appropriate at that time. By now,
I know people have changed and switched places, and everything is different so
I won't be at risk of exposing anyone.

### The end of the honeymoon
In October 18th 2016 my first daughter was born, and it was also the day that
marked the beginning of the end for me at that company. See, here in the UAE, the
paternity leave policy at that time was 1 week off, that company gave 2 weeks
and nothing more. Everyone knew my wife and I are here alone, the nearest
relatives we have are in Spain so we were becoming parents all by ourselves,
which is a lot of work, paternity leave is not vacation it's time to support
your wife who just pushed a human being out of her body and now has to breastfeed,
which is very energy-consuming and demanding.

So, 2 weeks are not enough, 40 days is the recommended resting time for new moms,
I literally had to beg for my line manager to approve 2 extra weeks off from my
annual leave and coming back, everything was changed.

My line manager started making my life impossible, I got a terrible performance
review, which I felt was not fair because an incident from over a year and a half
ago was brought up as the reason even though I fixed it in less than 1hr since the
bug affecting subscriptions experience was reported.

Then, my in-laws visited in November, they were staying with us in Dubai for
three months, until January, so, I applied for 2 weeks off in December so that
we could spend some time around Christmas and New Year together and show them
around. In December, the guy announces the following: *guys!, there's a lot to
do, so I won't be approving any vacations this month*, my time off request was
rejected and then I got to know another colleague got it approved to go back home
for a relative's wedding and another colleague was also traveling the same month.
I didn't get to spend too much time around either because there was always something
to do, one day, at around 17:30, the guy in question starts packing to leave and
says *Israel!, please make sure you close all the zendesk tickets before leaving*.
After he left, I checked our support dashboard... over 30 tickets pending for 
resolution. I was effectively being given all the crappy tasks no one else wanted
to work on.

### Then I was moved to a different team
Yup, I thought things were going to improve, and they did for a bit, in 2017 I
spoke in 2 conferences and several meetups, mostly about things I was working on
and trying to attract more talent for the company as well. Before one of the 
conferences I got a task on a very short notice before traveling, so, I made it
clear I might not be able to finish it before flying, but I would complete
what I could and leave pointers on how to finish it so someone else could pick
it up and I did exactly that, the night before flying I wrote a very comprehensive
email explaining the approach I was following and detailing which classes needed
to be changed, which methods and why, even left some test cases written to validate
the functionality worked correctly. Coming back from the conference, my new manager
calls me for a meeting, I thought it was something related to the talk I gave
in the conference, but nope... not even close...

He hands me a warning letter, on which grounds?, because *I failed to finish
a task and left for a week*, and he also mentioned *your team mates say you're
unreliable and they avoid working with you*, I was shocked, no one ever told me
anything like that. A few days after that meeting I asked my colleagues for
feedback and everything I got was positive, so, I figured, either my line manager
was lying to me or my colleagues were not professional enough to speak to me
directly about work-related things they were not happy about with me. In both
cases, I think that's unacceptable in a professional environment, I thought I left
all that "talking behind your back" thing back in high school.

Anyways, the performance review time comes and I'm called for the performance
evaluation meeting, to my surprise, there's my line manager and my old manager
sitting behind the table which I found strange, what was even more strange was
that the person delivering the performance evaluation was my old manager and,
guess what, it wasn't a good one and quite old things from over 2.5 years ago
were brought up, I have to admit I lost my cool in the middle stopped the
evaluation, told my manager *You'll have my resignation email in a few minutes*
and left the room, that's when I wrote an email to HR CCing my line manager to
start my notice period.

### Lesson learned
The minute you feel something is not right, speak up, complain, talk to people,
highlight it... if you feel it isn't right, then it isn't right, you don't have
take crap from anyone and if someone is giving you a hard time for no apparent
reason, the best thing to do is to part ways, to be honest, looking back I feel I
took too long to leave.

## And I joined Careem
I already wrote about joining Careem and why I joined, so, I won't repeat myself
here, but I will point out that I had my second daughter recently (2020) and I was
super scared about it, this didn't go too well with my previous employer and
I have to confess I started interviewing with several companies at the time, had
2 very good offer letters in my hands and was on the final stages for another 2
openings.

### Things were different this time
Well, the only thing which was the same was the paternity leave time, only 2 
weeks, I spoke to my manager about getting an extended paternity leave because 
of COVID, no family was going to be able to come over, airports were closed
(it was by the end of 2020) and forget about traveling to Spain and working from
there for a while... you know... because COVID... and also because my wife's 
passport was expiring. Nothing like this was ever proposed at Careem and he was
quite new in this *being a manager* thing so, he did what any new manager would do,
he asked me to go to HR and he would be OK with whatever they decide and, if they
said *no*, we would find a way.

So... I went to HR and explained everything, they were super supportive, they said
*hey, there's no such thing as an extended paternity leave, but you can request
time off your annual leave as we have unlimited vacations policy and we will leave
a note highlighting this was for paternity extension*. I did just that, not only
HR and my manager were super supportive, but also my whole team. I got a bit over
a full month to spend at home supporting my wife and taking care of our 4yo
who was staying at home because of the pandemic so that my wife could breastfeed
peacefully, also doing a lot of diaper changes.

Then, coming back to work was super flexible, they gave me quite flexible timing
to continue supporting at home while we got back into the routine. I even got a 
great performance review, I am beyond happy about all of this.

### What about the interviews
Well, I understood that very few companies are like this, so... I ended up rejecting
the offers and canceling the last interview rounds.

## My take about everything
A job is a job, and there's nothing you can do about it, you have it today,
tomorrow no one knows. But not all companies are the same, there are some which
puts the business first. For example, the first company I worked for... maybe they
needed someone cheap to complete the mechanical work of implementing web scrapers
to extract the content from those websites and they just stopped caring after it
was done, I don't know. The one where I did my internship, well, they had projects
to finish for the clients, so, they needed me to work on that, so my thesis and
my graduation goal wasn't a priority for them. My first job in Dubai, well, I
don't know what happened there, but if they had a "people first" kind of philosophy
nothing of that would had happened. Of course, I understand, if the business comes
first, it runs, it generates value and revenue and people gets paid.

Then, on the other hand, we have companies like Careem, maybe supporting my wife
is not their business or priority, but it was something I needed to do so, they
supported me on that. Then, coming back to work and get back into the routine
might be their priority, and also mine, but also family comes first, so... if 
something was happening at home, I had to solve that first and then comes the work.
They could have rushed me into work, but they didn't, we went step by step and
I took only non-critical tasks for a while until everything was settled both at
home and work. They understand that if you put your people first, they will 
care for the business as if it was their own, and that's exactly what I do. I want
to work on a place like this for a very long time, and it's my job to contribute
to its success so I can continue here.

Thank you Careem for being Careem.
