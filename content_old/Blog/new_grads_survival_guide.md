Title: New Grads Survival Guide
Date: 2016-03-25
Author: Israel Fermín Montilla
Tags: career

OK, OK... I'm sorry, I lied on the title, but please, don't stop
reading. I know... I know, we started bad our relationship already but,
I can explain it, I swear.

Yes, it says "survival guide", but it's not like that, I'm not going to
tell you a recipe to get the job you want, in the company you like and
the country you dream, that's impossible, as programmer we all know
there is no silver bullet, and it applies not only on software, also in
day by day life problems.

What I'm going to share with you, is what worked and still working for
me, let me give you some context. I'm a 29 years old Computer Engineer
from Venezuela, yes, I know... we're famous for good baseball players,
beautiful women and, most recently and sadly, thanks to this guy.

![Hugo Chávez, worst Venezuelan
president...](https://dl.dropboxusercontent.com/s/8v0mswsxcdii5pd/chavez.jpg)

And this other guy...

![Nicolás Maduro... even worse that
Chávez](https://dl.dropboxusercontent.com/s/2yqy0xbwldnwth4/maduro.jpg)

My country is, obviously, a 3rd world country, we're facing huge and
very serious economic and political issues and it's quickly getting
worse thanks to those guys, my family is not rich so I couldn't travel
or go to another country to study until things get better and, by now,
I'm living in Dubai, paying my rent and brought my wife abroad so we
have some hope of having the quality of life we dreamed together, how I
did it?, my job is my flight ticket and my passport.

I managed to get out of Caracas, one of the most violent and dangerous
cities in the world, and come to Dubai 2 years ago. Obviously this
wasn't with money from my own pocket, a local company hired me and
covered my relocation expenses. I'm going to share with you some things
I did and still doing, this is not guaranteed to give you the job you
want, but certainly will help you increase your chances.

-   *Set up a decent LinkedIn profile:* no joke, this helps, a lot of
    recruiters and companies in LinkedIn, don't underestimate that tool,
    it's really powerful and you don't need to pay for the pro or
    premium version. A good LinkedIn profile includes a picture, a
    description of each job you had, and by description I mean
    responsibilities and key achievements and responsibilities in
    that position.
-   *Have a public GitHub account:* and actually *use it!*, I'm not
    telling you to contribute to a lot of open source projects or to
    start yet another social auth library, just experiment with new
    tools, build some cool hello-world style projects and upload the
    code to GitHub, this will show that you're actively learning new
    stuff all the time.
-   *Have a StackOverflow account:* and, again, *use it*, answer some
    questions and ask some questions, it's useful and it will help you,
    trust me.
-   *Be passionate:* love your career, you studied it for a reason.
    Learn new stuff, experiment with a new language or framework, show
    everybody the cool stuff you're working on, get ideas from other
    people and learn from them.
-   *Be pragmatic:* yes, when it comes to work, you have to let your
    knowledge and logic guide your decisions, not your emotions.
-   *Choose your weapon:* and do it early, it's good to be versatile,
    you're a programmer, not a user of a programming language. But you
    need a battle ax, the one that helps you solve any problem faster
    and you feel more comfortable with.

And finally, the one I consider the most important.

-   *Choose your first job carefully:* yes, I mean it, don't let the job
    choose you, you have to enjoy your job because it's where you will
    spend most of the day and, trust me, your first job will define your
    career. If you start developing J2EE applications, people will hire
    you to do so, because that's the experience you have. Don't be
    afraid to say no to an offer you're not totally convinced of taking.
    In my case, when I was looking for an internship, I turned down an
    offer from Microsoft in Venezuela because I've always been an Open
    Source enthusiast and a Linux user, I didn't found myself
    programming in ASP.NET, I decided to take an offer from Vauxoo, a
    local small company that developed open source modules for OpenERP
    (now Odoo) and I'm very happy I took that road, I learned Python in
    that job, which is my battle ax and my main tool by today.

So... yeah!, not a recipe, but some tips I hope will help you on your
first years as a professional.
