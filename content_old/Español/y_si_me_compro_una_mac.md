Title: ¿Y si me compro una Mac?
Date: 2014-10-03
Author: Israel Fermín Montilla
Tags: personal

Llevo ya un par de meses considerando la posibilidad, sí, YO, Israel
Fermín Montilla llevo un par de meses considerando la posibilidad de
comprarme una *MacBook*.

No, no me hackearon github y subieron este artículo, tampoco estoy
sufriendo de algún tipo de enfermedad mental, mucho menos considero que
he aprendido todo lo que hay que aprender de Linux, tampoco me fastidié
de Linux, simplemente estoy considerando la posibilidad.

En Venezuela era impensable e incomprable, pero acá, es algo que puedo
hacer, podría fácilmente adquirir una MacBook Pro retina display,
instalar homebrew y usarla también como máquina de desarrollo, ¿por qué
no probar algo diferente?

![Problem?](https://dl.dropboxusercontent.com/s/aeyb7a5txzkqw2d/thinkdifferent.jpg)

Siempre he sido un crítico de *Apple* y de todo lo que representa,
siempre he dicho que es una fábrica de juguetes y que el único juguete
que sirve es el iPod Classic, porque es fácil de usar, cumple su
cometido muy bien y le cabe una cantidad grosera de música, el mío es de
160GB y tengo **todos** mis discos allí metidos y aún le cabe más
música. También siempre me he burlado de los fanáticos de *Apple*, les
digo *iSheeps* porque, básicamente, así se comportan.

![iSheeps en
formación](https://dl.dropboxusercontent.com/s/2eygv1qqyzjf7h2/isheeps.jpg)

Yo no soy una oveja, creo que soy mejor que eso, pero luego, pensando al
respecto, tener una Mac, no me hace *iSheep*, comprar todo lo que tenga
la manzana mordida y ciegamente decir que es superior aunque
técnicamente sea falso sí, al fin y al cabo, como Ingeniero y como
programador, siento que debería aunque sea probar para definir si es
bueno o malo.

Algunas de las ventajas que veo respecto de tener una Mac son:

-   **Software que funciona:** no es que Linux no funcione es sólo que a
    veces requiere de algunos pasos adicionales de configuración para
    que funcione como debe ser, en Mac simplemente instalas y ya, igual
    que en Windows.
-   **Sistema operativo estable:** no es que Linux no sea estable, es
    que Windows, la otra alternativa en cuanto a sistemas operativos
    "mainstream", no lo es tanto, acá Linux y Mac, parecieran estar a la
    par pues ambos son sistemas Un\*x (o eso es lo que nos hacen creer).
-   **Compatibilidad:** bueno, no es que en Linux haya problemas con
    drivers ¿verdad?, uno siempre logra hacer funcionar las cosas, a
    veces cuesta un poco más de tiempo, pero no hablemos de drivers y
    software técnico, la mayoría de los entornos de desarrollo y
    servidores de base de datos, storage no-sql, etc funcionan perfecto
    en Linux, hablemos de software para uso diario, *Skype*, el plugin
    de *Hangouts* para *Chrome*, estemos claros que *LibreOffice*
    apesta, hay muchas alternativas para producir documentos, incluso de
    mayor calidad que con *Word*, *PowerPoint* o incluso *KeyNote*, está
    LaTex, por ejemplo, y hay miles de librerías para producir
    presentaciones excelentes usando HTML, y JavaScript, pero muchas
    veces uno simplemente quiere hacer algo rápido, sin necesidad de
    escribir mucho código. Skype, por ejemplo, es imposible de instalar
    en *Debian*, el paquete está corrupto, el de *Ubuntu* y *Linux Mint*
    funciona perfecto, lo mismo que con los plugins de *HangOuts*,
    incluso el plugin de *Flash Player* me ha dado problemas en *Ubuntu*
    luego de un tiempo.
-   **Software de grabación:** todos saben que toco guitarra, me
    gustaría empezar a grabar las cosas que compongo, *Audacity* apesta
    y *Ubuntu Studio* a veces me deja mal. He visto mucha gente trabajar
    con *Garage Band* y hacer cosas geniales.

Esas son algunas de las razones que consigo como para una eventual
migración, según tengo entendido, son geniales como máquinas personales
y para realizar trabajos multimedia, bueno, me consta porque mi hermano
es diseñador gráfico y todo lo hace allí, igual en varios estudios de
grabación (claro, ellos usan *Pro Tools*).

Mi mayor preocupación al respecto es la ausencia de un gestor de
paquetes nativo, no puedo simplemente hacer *aptitude install* y que
todo funcione de maravilla. Todos los usuarios *Mac* en la oficina me
dicen que *homebrew* hace el trabajo bastante bien, incluso, mi pana de
[PyVE](http://python.org.ve) [Wil
Álvarez](https://github.com/satanas) me dice que
es algo que vale la pena probar y que está súper feliz programando en su
*MacBook*.

Simplemente lo estoy considerando, necesito leer más al respecto, quizás
jugar un par de días con una a ver qué decido hacer.
