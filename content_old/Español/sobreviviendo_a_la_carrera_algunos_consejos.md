Title: Sobreviviendo a la Carrera: algunos consejos
Date: 2011-09-02
Author: Israel Fermín Montilla
Tags: carrera

Bien, si estás en los últimos semestres de Ingeniería Informática, eso
significa que has sobrevivido al ataque de los cálculos, lograste
controlar la rebelión de las físicas, dominaste con experticia el asedio
de los algoritmos, batallaste arduamente y, ahora, sólo quedan un par de
ciclos por cerrar al culminar la carga académica: Servicio Comunitario,
Tesis y Pasantía. Mucha gente (incluyéndome) suele sentir algo de miedo
al llegar al 9no semestre de la carrera, pues significa que debes
empezar a pensar en ciertas cosas: ¿en qué área te ves trabajando? ¿de
qué vas a hacer tu trabajo especial de grado? Más aun: ¿tesis o pasantía
larga? ¿dónde voy a hacer mi pasantía corta? Muchas cosas en mente y muy
poco tiempo si son como yo, que dejé todo eso para última hora. Pero no
desesperen, todo tiene una solución, es sólo cuestión de actitud, por lo
que me tomaré la libertad de ser un poco informal en este artículo. Voy
a tratar de darles algunos consejos basándome en mi experiencia personal
en esa etapa (reciente, por cierto) de mi vida: A lo largo de la
carrera, deben preocuparse por no ser "sólo estudiantes". Está bien
dedicarse a la carrera ---y, si son "Eficiencia 1", pues, mucho
mejor---, pero la magia de todo está en tratar de compaginar esa
excelencia académica con otras actividades en las que puedan desarrollar
otras aptitudes y aprender nuevas herramientas. Por ejemplo, yo soy una
persona sumamente tímida, bastante callado a decir verdad; a veces me
cuesta interactuar con otras personas (puede ser un mal de carrera).
Eso, en el campo profesional, no es muy deseable que digamos, sobre todo
porque hay una entrevista de trabajo en la que deben desenvolverse bien
si quieren obtener el empleo, muchas veces nos tocará trabajar en equipo
con otros desarrolladores y líderes de proyecto o, quizás, interactuar
con clientes (miedo, terror, ¿pánico?). ¿Cómo hice para sobrellevar eso?
Pues la respuesta es simple: enfrentándolo. Me inscribí para ser
preparador de mi escuela, en la cátedra de Algoritmos y Programación II,
ya que allí se estudian estructuras de datos en Lenguage C, uno de mis
favoritos, por lo que pensé que podría hacerlo bien. Al final del
semestre, no me fue tan mal en las encuestas y recibí muy buenos
comentarios, además, me empecé a sentir más cómodo hablando en público,
así que empecé a inscribirme en otras actividades:

-   Centros de Estudiantes y Representación Estudiantil: en la temporada
    de campañas, uno habla con mucha gente y debe debatir con otros
    candidatos, eso me ayudó a desarrollar un poco de coherencia en el
    discurso y a mejorar la capacidad de acercarme a la gente. Fui
    Consejero de Escuela y Consejero de Facultad, eso también me ayudó
    con mi problema para hablar con la gente.
-   Sun Team University e IEEE-UCAB: como miembro de esas sociedades
    técnicas estudiantiles, debía dar charlas, lo que implicaba hablar
    en público. Pertenecer a estas organizaciones me permitió
    desarrollar aptitudes para exponer y explicar temas técnicos de mi
    elección a un público. Muy parecido a ser preparador, pero, esta
    vez, el contenido y todo el esquema lo seleccionaba YO.

Participar en actividades de este tipo también te acerca un poco a las
autoridades de tu escuela y de tu facultad, de quienes puedes aprender
muchísimo y a quienes puedes acudir en caso de necesitar orientación
académica de algún tipo o un consejo.

Cuando digo que traten de no ser "sólo estudiantes", también hago
referencia a que no está mal conseguir un trabajo de medio tiempo o a
distancia. Tuve la suerte de trabajar en
[Es|Noticia.com](http://www.es-noticia.com){.reference .external}, un
concentrador de noticias donde aprendí un enfoque distinto de los
desarrollos web, también en el IAMJ (Instituto Autónomo Metropolitano
para la Juventud), un organismo gubernamental adscrito a la Alcaldía
Mayor (cuando existía) en el que aprendí que si te dicen que serás el
Web Master y además, eres el único en la parte de IT, debes tener
cuidado. Terminarás configurando servidores y redes, montando la
intranet, instalando Windows, lidiando con todos los virus y "errores de
capa 8" del lugar y trabajando en la página web en sus ratos libres en
la Universidad o en casa. Todas estas cosas les darán la experiencia que
no tendrán simplemente asistiendo a sus clases programadas, estudiando
para los exámenes previstos y desarrollando los proyectos asignados,
pero todo lo anterior les dará los conocimientos y las aptitudes para
poder salir a ganar experiencia en la calle así que siempre una cosa
viene a complementar a la otra. Finalmente, las temidas obligaciones de
final carrera: Tesis, Pasantía y Servicio Comunitario: Para el Servicio
Comunitario, piden 120 horas. Traten de hacer algo que no les consuma
mucho tiempo y en lo que no vayan a ganarse un "contrato vitalicio de
mantenimiento gratis" ciertamente hay muchas organizaciones sin fines de
lucro que necesitan urgentemente una página web, un sistema de gestión
de &lt;inserte aquí cualquier cosa gestionable&gt;, pero muchas veces no
saben lo que eso implica y se aprovechan de la necesidad del estudiante
por terminar el servicio comunitario así que, si se embarcan en uno de
estos proyectos, asegúrense de dejar todos los puntos claros y un
alcance bien definido desde el principio. La pregunta eterna: ¿Tesis o
Pasantía Larga?. Considero que es una decisión muy personal.
Particularmente pienso que como estudiantes de una carrera de corte
científico, como lo es la Ingeniería Informática, un Trabajo Especial de
Grado (Tesis o TEG) debería ser la única opción, existen demasiados
temas de investigación poco explorados, muchas tecnologías que no
estudiamos durante la carrera y la tesis es una oportunidad excelente
para explorarlos. Algunos consejos para seleccionar un tema de tesis:

-   No tienen que descubrir el agua tibia: recuerden que es una tesis de
    pre-grado, si bien hay que ser innovadores, no se trata de poner un
    satélite en órbita o volverse un experto en derivación de forlayos
    para tener un buen trabajo de grado.
-   Los temas de Inteligencia Artificial Bioinspirada son muy buscados
    y, además, ya existen muchos trabajos en esa área, incluso en
    pre-grado. Traten de buscar otros temas interesantes: Social Media,
    Web Semántica, Data Mining, Web Mining, Orquestación de Servicios
    Web, Procesamiento de Lenguaje Natural. Alguna manera de acercar el
    conocimiento académico al mundo real, por ejemplo.
-   No tengan miedo en desarrollarla solos, si no consiguen pareja para
    desarrollar su tesis, no se ajusten a lo que quiera hacer otra
    persona, al final la gran satisfacción radica en trabajar en los que
    nos gusta o nos apasiona. En mi caso, mi tesis la desarrollo de
    manera individual, en el área de Web Semántica pues me parece un
    tema súper interesante y no hay antecedentes de trabajos en esa área
    en mi universidad (UCAB Caracas).

Finalmente, la pasantía, para muchos, será su primer acercamiento al
mundo laboral, otros quizás tuvieron algunos empleos o desarrollaron
algún sistema por su cuenta durante la carrera (Metodología del Software
en la UCAB, ¿alguien?). Lo importante de la pasantía es conseguir un
lugar donde puedan crecer profesionalmente y donde puedan tener la
oportunidad de quedarse trabajando. Muchas empresas buscan pasantes para
terminar desarrollos internos o para realizar trabajos que más nadie
quiere hacer y luego, simplemente, olvidarse de ellos. Obviamente, si lo
que quieren es aprender y crecer en el proceso, esta no es una opción.
Algunos consejos para la pasantía:

-   Busquen más que una pasantía, traten de conseguir un lugar donde
    puedan quedarse trabajando y déjenlo claro en la entrevista. En mi
    caso, siempre dije que en realidad estaba buscando trabajo, mi
    visión de la pasantía es la puerta de entrada al mundo laboral. Si
    dicen que sólo quieren hacer pasantía, pueden pasar
    por conformistas.
-   Durante la pasantía, hagan su trabajo lo mejor posible. No tengan
    miedo de preguntar algo si no saben, y tómense su tiempo para
    entender las cosas y desarrollarlas lo mejor posible. Es mejor
    calidad que cantidad. Además, lo más seguro es que en realidad
    manejen el concepto, pero no sepan aplicarlo correctamente de manera
    práctica lo cual, usualmente para los jefes, es comprensible en
    alguien que está empezando en el mundo laboral.
-   Siempre es bueno aprender cosas nuevas, pero soy de los que piensa
    que, primero, debemos perfeccionar nuestras habilidades con las
    herramientas con que trabajamos a diario. Si trabajan con un
    lenguaje de programación determinado, investiguen y exploren qué
    otras posibilidades ofrece; enfóquense primero en lo que utilizan en
    la oficina, esto les permitirá completar sus obligaciones de manera
    más rápida y les dejará más tiempo libre para aprender nuestas
    herramientas (desarrollo de videojuegos, otros frameworks para
    desarrollo web o quizás algún proyecto personal).
-   Envíen su hoja de vida a varias empresas, sin importar cual sea,
    muchas veces la vida da sorpresas y el lugar que menos se imaginan
    es el que les ofrece su trabajo ideal.

En mi caso, cuando estuve buscando pasantía, envié mis papeles a
Microsoft, IBM y Vauxoo. En Microsoft me ofrecían participar, como
pasante, en un desarrollo interno, básicamente el proyecto era
automatizar la organización del material corporativo y académico de
Microsoft de Venezuela, más allá de eso, me ofrecían la figura de
"Pasantía Corporativa", que, en pocas palabras es "El pasante eterno",
por otra parte, Vauxoo me ofrecía una oportunidad real de trabajo y el
apoyo para introducir mi primer proyecto como pasantía en la
Universidad. Hoy resulta obvio que decidí trabajar en Vauxoo, la razón
principal es que es una empresa que trabaja 100% son Software de Código
Abierto. Al trabajar con herramientas OpenSource y aprovecharse de ese
ecosistema, no hace falta tener mucho sentido común para darse cuenta de
que lo moralmente correcto es retribuir de la misma manera, por lo que
cada línea de código que se escribe en Vauxoo, es liberada a través de
[Launchpad](http://launchpad.net/~vauxoo). Además,
me ofrecían trabajar con un lenguaje de programación distinto (Python) y
al que siempre le había tenido el ojo puesto y nunca, en la universidad,
tuve la oportunidad de dedicarme aprender. Esta oportunidad se adaptaba
más a mi perfil y fue por ello que, al final, decidí rechazar la
oportunidad en Microsoft. Al final, lo que importa es que se sientan
cómodos en el lugar donde estén haciendo la pasantía y, si esto es así,
que la empresa pueda ofrecerles la oportunidad de quedarse trabajando si
consideran que su trabajo es de calidad y siempre llevar una toalla, uno
no sabe si el mundo se acaba antes que la carrera.

<div class="article-footer">

<span class="glyphicon glyphicon-tags" aria-hidden="true"></span>
[carrera](tag/carrera.html) [estudiante](tag/estudiante.html)
[informática](tag/informatica.html) [opinión](tag/opinion.html)
[universidad](tag/universidad.html)

<div class="social-buttoms">

<div class="social-link">

[Tweet](https://twitter.com/share){.twitter-share-button}

</div>

<div class="social-link">

<div class="fb-share-button"
data-href="http://iffm.me/sobreviviendo-a-la-carrera-algunos-consejos.html"
data-layout="button">

</div>

<div id="fb-root">

</div>

</div>

</div>

Thank you for reading!, don't forget to subscribe to my mailing list!, I
don't send any spam and sometimes I do share interesting stuff

<div id="mc_embed_signup">

<div id="mc_embed_signup_scroll">

<div class="mc-field-group">

Email Address

</div>

<div id="mce-responses" class="clear">

<div id="mce-error-response" class="response" style="display:none">

</div>

<div id="mce-success-response" class="response" style="display:none">

</div>

</div>

<div style="position: absolute; left: -5000px;" aria-hidden="true">

</div>

<div class="clear">

</div>

</div>

</div>

</div>

<div id="disqus_thread">

</div>

</div>

</div>

</div>

<div class="footer">

<div class="container">

<div class="row">

<div class="col-xs-8">

Created by [Israel Fermín Montilla](http://iffm.me) using
[Pelican](http://getpelican.com) and
[Bootstrap](http://getbootstrap.com). All content here is under
[Attribution-ShareAlike 3.0
Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en_US)
license. Logo designed by [Andrés Fermín](https://about.me/andresfermin)

</div>

</div>

</div>

</div>
