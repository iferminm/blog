Title: Algo que escribí antes de emigrar
Date: 2014-11-20
Author: Israel Fermín Montilla
Tags: reflexion, personal

Revisando archivos viejos luego de que migré a *Linux Mint*, conseguí
algo que escribí unos 3 días antes de emigrar, recuerdo haberme sentado
en mi escritorio, aún sin poder creer que me iba de casa, que me iba de
mi país, sintiéndome muy impotente de toda la situación que me había
obligado a tomar esa decisión, como he dicho varias veces, a nadie le
gusta estar lejos de sus seres queridos. Recuerdo que encendí la
computadora, inicié *vim* y simplemente empecé a escribir, quiero
compartir acá con ustedes lo que escribí esa noche antes de acostarme a
dormir con la ansiedad del viaje que me esperaba.


## Esto fue lo que escribí

La verdad, es que me describo como una persona poco emotiva, quizás un
poco seca y poco sentimental. Varios de mis amigos se han ido,
familiares, incluso, están haciendo vida ya fuera del país que lo tiene
todo y al mismo tiempo no tiene nada y, en sus respectivas despedidas,
estuve siempre tranquilo.

Todo cambia cuando el protagonista de la despedida es uno, me jacto de
siempre hacer valer la lógica por encima de los sentimientos al mejor
estilo vulcano pero, ante ciertas situaciones, no hay manera de evitar
que el lado humano aflore, ni Spock está excento de sucumbir ante sus
emociones de vez en cuando.

Mientras preparo las maletas, pongo a punto mi bolso de mano con todos
los documentos que voy a necesitar durante el viaje de casi 24 horas, no
puedo evitar, en mi cuarto, ver todos los adornos, cosas que me han
regalado y, en mi mente, revivir esas anécdotas, ex-novias que me han
dado cosas que aún conservo, buenos amigos que me han obsequiado otras
y, lo más importante, las cosas que me ha dado mi prometida que siempre
mantengo a la vista. Historias detrás de los objetos que permanecen en
las repisas de mi habitación, inertes, testigos de cómo voy tomando
algunas que quiero llevarme y metiéndolas en mi bolso mientras, otras,
permanecerán allí, inmóviles, agarrando polvo esperando que nos veamos
de nuevo algún día.

Hablar del tema con mi familia, me afecta un poco, quizás bastante. Me
voy a un lugar “no-tan-cercano”, donde hay que tomar como mínimo dos
aviones para poder llegar finalmente al destino, donde el pasaje es muy
costoso como para darse el lujo de “ir de vez en cuando a visitar” y más
aún con la situación actual de mi país. Veo a mi abuela con los ojos
aguados, tratando de no llorar, mi tía de crianza, Carmen Carrillo,
también conteniendo las lágrimas y deseándome buen viaje. Mi mamá, mi
papá y mi prometida, tratando de aprovechar al máximo estos últimos días
juntos, días en los que decidí poner de lado mis proyectos para
dedicarme a estar con mi familia y terminar ciertas diligencias para el
viaje. Días en los que me doy cuenta de los obstáculos y las trabas que
ha colocado el gobierno para restringir aún más la salida de venezolanos
a otras tierras, aún cuando sea sólo por placer, salir de Venezuela es
un privilegio y un lujo que sólo algunos pueden darse mientras vemos a
los familiares de quienes hoy detentan el poder conociendo todo el mundo
y gozando de tantas comodidades que parece absurdo, no puede haber otra
reacción más que incrementar mi alivio por salir y mi repulsión hacia
quienes han causado tanto daño.

Mi nombre es Israel Fermín Montilla, tengo, a la fecha, 27 años de edad,
VENEZOLANO de nacimiento y de corazón y para el momento de escribir
esto, faltan 3 días para irme del país, en búsqueda de un mejor futuro
para mi, para mi prometida, que en diciembre será mi esposa, y para los
hijos que planeamos tener, los nietos que queremos darle a nuestros
padres. Lamentablemente, en la Venezuela actual hay pocas oportunidades
de desarrollo, la posibilidad de independizarse, salir de la casa de los
padres y tener uno su propia vivienda es casi, por no decir, totalmente,
nula. He visto a mis amigos partir, quienes me conocen saben que he
luchado hasta donde he podido, pero me apena confesar que me venció el
cansancio, ¿qué pensaría mi abuelo hoy de mi?.

En 3 días estaré viajando a un lugar donde seré un completo extraño,
perdóname Venezuela, por dejarme vencer por el fantasma del cansancio y
dejar de nadar contra la corriente para llegar a rescatarte, espero que
todo mejore pronto y se que cuando decida volver, me recibirás con los
brazos abiertos como buena madre.

Familia, amigos, compañeros, conocidos… Hasta siempre!!

-- Israel.
