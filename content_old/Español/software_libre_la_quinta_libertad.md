Title: Software Libre: La Quinta Libertad
Date: 2011-07-03
Tags: software libre
Author: Israel Fermín Montilla

Esta entrada será corta. En el artículo anterior, nombré las 4
libertades fundamentales de las que gozamos los felices usuarios del
Software Libre, desde la libertad 0 hasta ha libertad 3.

Pero olvidé una muy importante que, si bien no es oficial, siempre la
agrego cuando hablo de este tema. Hablo de la quinta libertad, la
libertad 4 (recuerden que empezamos desde 0), la Libertad de Elegir,
libertad de seleccionar qué queremos utilizar, ok, tenemos un sistema
operativo: Linux, pero tenemos además cientos de distros disponibles
para escoger cuál queremos, gestor de paquetes dpkg o basados en rpm,
ok, pero ahora quiero algo listo "out of the box" o algo que pueda
configurar y adaptar, bueno, ahora, ¿qué interface gráfica quiero?:
¿KDE?, ¿Gnome?, ¿XFCE?, ¿Fluxbox?, ¿realmente quiero utilizar una GUI?.
Esta historia se repite desde para seleccionar un reproductor multimedia
(Rythmbox, VLC, MPlayer, Amarok), hasta para seleccionar algo tan simple
como un editor de texto plano (gedit, kate, emacs, vim), tenemos cientos
de opciones de donde poder escoger, eso es Software Libre.
