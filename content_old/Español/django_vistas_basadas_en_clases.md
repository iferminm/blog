Title: Django: vistas basadas en clases
Date: 2015-07-25
Author: Israel Fermín Montilla
Tags: django, python, vistas basadas en clases, cbv

Tenía pendiente escribir sobre esto desde hace tiempo, pero entre una
cosa y otra siempre terminaba escribiendo sobre otra cosa y las vistas
basadas en clases quedaban olvidadas.

Las vistas basadas en clases nos permiten simplificar muchísimo el
código, reduciendo, para muchos casos, la escritura de vistas a
simplemente heredar de usas vistas genéricas y agregar algunos
[mixins](http://iffm.me/cosas-que-he-aprendido-parte-ii.html){.reference
.external}, son un recurso bastante potente y que deberíamos tomar en
cuenta en los proyectos que estemos o vayamos a desarrollar y que además
está allí desde django 1.3.

Cuando estaba empezando a escribir la introducción, me dió por revisar
el blog de [Álvaro Hurtado](http://alvarohurtado.es){.reference
.external}, un compañero de trabajo en dubizzle, conseguí varios
artículos que voy a compartir porque están bastante sencillos y fáciles
de entender y, además, en perfecto español, es raro que la gente decida
hacer un blog en español y mantenerse fiel sin escribir en inglés.

Acá los links:

1.  [Introducción](http://www.alvarohurtado.es/django-vistas-basadas-en-clases-i-introduccion/){.reference
    .external}: explicación breve acerca de las Vistas Basadas en Clases
2.  [DetailView](http://www.alvarohurtado.es/django-vistas-basadas-en-clases-ii-detail-view/){.reference
    .external}: ejemplos sobre la vista de detalle
3.  [ListView](http://www.alvarohurtado.es/django-vistas-basadas-en-clases-iii-list-view/){.reference
    .external}: ejemplos sobre la vista de lista

Espero que sean de su agrado.
