Title: Graduado, ¿y ahora?
Date: 2012-06-27
Author: Israel Fermín Montilla
Tags: personal, reflexión

Bueno, vengo con otra reflexión respecto a qué ocurre al culminar la
carrera a nivel universitario, quiero empezar aclarando que acá expondré
mi (¿poca?) experiencia personal y este artículo estará marcado por mi
manera particular de ver las cosas que no necesariamente resulta ser la
de todos.

Realmente no hace mucho que salí de la Universidad al campo laboral
formalmente, aunque durante mis años de estudiante trabajé en un par de
cosas. Parte de mi filosofía y experiencia durante la carrera ya la he
escrito en un artículo anterior, y en otro bastante reciente, sin
embargo, muchas cosas van cambiando a lo largo del tiempo, no es que sea
un profesional muy experimentado, pero, espero que estas líneas le
sirvan a alguien para guiarse, eso si, sin echarme la culpa.

Particularmente, mientras estuve estudiando y trabajando, tuve un
pequeño complejo de inferioridad por no estar aún graduado, aquí en
Venezuela, como se dice en criollo, "la gente come mucho título", es
decir, sin el título no eres nadie, sin el certificado, no sabes. Ahora,
acabando de salir, el complejo es el mismo, pero por estar "recién
graduado". Esas dos etiquetas son temas sociales muy graves, he
conseguido TSU que saben mucho más que muchos Ingenieros que conozco y
estudiantes que pueden resolver un problema mejor que un profesional ya
graduado, por ello, no hay que acomplejarse por estar en alguna de esas
situaciones, al final, lo que le interesa a la empresa es alguien que
haga el trabajo y que pueda hacerlo bien, mientras, tanto las empresas
como los profesionales, no entiendan esto, el mundo laboral estará
plagado de piratas con título y gente brillante que, o bien está por
obtenerlo o, por alguna razón no les fue posible, la etiqueta de "recién
graduado", debería significar "soy joven y puedo aprender rápido,
contrátame" y no "no tengo experiencia, explótame".

Esos mismos complejos, me llevaron a aceptar trabajos con poca paga y
muchas responsabilidades, cobrar poco por desarrollar proyectos para
algunos clientes y no decir nada, ante esto, no me queda más que decir,
no se dejen explotar, sean pasantes o profesionales recién graduados, su
trabajo vale, no trabajen tiempo extra gratis, a menos que sea realmente
necesario o sea su culpa, respeten su horario de trabajo, sean los
primeros en llegar, pero también salgan a la hora, tienen familias,
amigos, novias con quienes compartir, quizás puedan dedicar parte de ese
tiempo, también, a algún pasatiempos o a desarrollar algún proyecto
personal o para algún cliente bajo la figura de "freelancer", utilicen
su trabajo como un gimnasio para poner el forma el músculo de
programación, para agarrar experiencia y conocimiento, pero no lo
apliquen sólo allí, citando a mi amigo Nhomar Hernández, "hay que
trabajar cada día como si fuera el último", pero no sólo en una cosa,
hay que diversificar.

Una vez graduados, pensamos que dejaremos de estudiar pero, en realidad,
apenas empezamos, cada proyecto puede enviarnos varios meses a wikipedia
o hacernos sumergir en una serie de libros y conceptos que, si bien
puede que no sean técnicos de computación, forman parte del contexto del
sistema y debemos manejarlos, por ejemplo, durante mi breve paso por el
mundo de los ERP cuando trabajé
en [Vauxoo](http://vauxoo.com/){.reference .external}, tuve que aprender
conceptos de contabilidad e inventarios, incluso características de
equipos de refrigeración para un proyecto, además de estudiar e
investigar muchísimo sobre las herramientas usadas en la empresa pues
eran nuevas para mi (Python y OpenObject, la plataforma de OpenERP). En
esta carrera, básicamente, nunca dejamos de estudiar y aprender,
actualmente, en mi actual trabajo
en [Zava](http://zava.com.ve){.reference .external}, me toca investigar
sobre la cultura italiana alrededor del mundo y las tendencias actuales
de la misma, además del framework utilizado para el desarrollo del
proyecto (Django). Finalmente, recuerdo algo que me dijo un buen amigo,
Tomás Henríquez: "los primeros empleos te definen, mosca con lo que
eliges", ciertamente, los primeros empleos definen quien serás
profesionalmente, por ello, si en los primeros tres trabajos programaste
en Java o en PHP, o administraste servidores, muy probablemente para el
cuarto te busquen para hacer eso mismo.

Ciertamente, en el mismo orden de ideas de Tomás, "es preferible pasar 3
o 4 meses desempleado que trabajar en algo que no te gusta". El proceso
de hacer tu "plan de carrera" debe comenzar antes de graduarte, debes
pensar qué quieres hacer, en mi caso, quiero especializarme en
Desarrollo de Software, un campo muy competitivo, por ello, seleccioné
herramientas que no fueran muy comunes en el área, de esta manera te
diferencias del resto, claro que se programar en PHP y Java (en .NET no
por un tema de principios) pero todo el mundo programa en PHP, .NET y
Java, por lo que en un primer momento seleccioné Perl como el lenguaje
en el que me especializaría, empecé a hacer tutoriales, leer libros y
hacer algunos programas utilizando las librerías del CPAN, esto cambió
cuando entré en [Vauxoo](http://vauxoo.com/){.reference .external} y
aprendí Python, mi actual hacha de batalla, por ahora, mi plan de
carrera es especializarme en Desarrollo Web, en todos sus campos,
Back-End y Front-End: desde servicios web y web semántica, hasta RIA, un
buen programador debe ser versátil y adaptarse a cualquier parte del
proyecto. Para cerrar, quiero aclarar que con esto último no estoy
diciendo que se casen con una tecnología o un lenguaje en particular,
aprendan de todo un poco, sean buenos, pero hay que especializarse y ser
excepcionalmente bueno en algo, yo elegí Desarrollo en Python, y estoy
trabajando en ello, también programo en PHP, Java, Perl y C/C++ si es
necesario, pero mi principal herramienta es Python y, aún así, tengo
intenciones de aprender Ruby.
