Title: Empezando a conocer la Web Semántica (Parte I)
Date: 2011-05-10
Author: Israel Fermín Montilla
Tags: web semántica, web 3.0, internet

Mucho hablamos hoy día de lo "dos punto cero": en internet, en la radio,
en la televisión, se habla incluso de programas y empresas "dos punto
cero" que se apoyan en estas tecnologías de la web para comunicarse con
su audiencia o bien para ser transmitidos (podcasts, videocasts, radios
por internet).

Y es que la Web 2.0 ha revolucionado desde la manera en que vemos la
Internet hasta el enfoque de negocios de muchas grandes empresas. Pero,
en este mundo cambiante de la tecnología, la Web 2.0 no es más que un
estado transitorio a otro más sofisticado: la Web 3.0, conocida en la
inmensidad de Internet como la Web Semántica.

Resulta conveniente refrescar la memoria con algo de Historia de
Internet pues, si actualmente nos encontramos en la Web 2.0, en algún
momento debió existir una Web 1.0. Nos remontamos a los inicios de la
Autopista de la Información, recordamos aquellas páginas frías,
estáticas y en las que únicamente personal altamente capacitado y
especializado era capaz de crear contenido y publicarlo, lo que se
traducía en que sólo las grandes empresas tenían presencia en la red.
Además, el flujo de información era unidireccional, es decir, se
generaba un mensaje y otros, los usuarios, eran importantes en la medida
en que se convertían en consumidores de dicha información. Poco a poco
la tecnología fue evolucionando, aparecieron lenguajes capaces de
procesar y pre-procesar hipertexto con los que se puede lograr sitios
web más dinámicos y capaces de interactuar con bases de datos, con lo
que el contenido pasó de ser de estático a cambiante. Esto trajo consigo
que, aún cuando sólo personas con conocimientos de computación y
programacion son los capaces de crear sitios y espacios en la web, todos
los usuarios son capaces de generar y publicar contenido, con lo que la
información ahora fluye de manera multidireccional, apareciendo
conceptos como Redes Sociales e Inteligencia Colectiva, de esta manera
nace lo que conocemos hoy día como La Web 2.0 en la que, contrario a la
Web 1.0, los usuarios son importantes en la medida en que son
generadores constantes de contenido.

Con esta nueva Web, cargada de contenido que, además, crece
exponencialmente segundo a segundo, cobran especial importancia los
buscadores. Estos buscadores, en su mayoría, trabajan mediante palabras
claves o "keywords" en inglés. De esta manera, un documento es relevante
según el número de veces que aparece una palabra clave y no por su
significado en el contexto de la consulta realizada por el usuario. Este
esquema funciona para búsquedas en las que el contexto no es tan
importante, por ejemplo "Internet en Venezuela", resulta ser una
consulta bastante general, podrían interesarme desde Proveedores de
Servicio de Internet en Venezuela, hasta estadísticas acerca del uso y
páginas más visitadas.

Supongamos que estamos planeando un viaje familiar a Los Andes, debemos
llegar a Valera, Edo. Trujillo, entonces nos vamos al buscador de
nuestra preferencia y consultamos: "Todos los vuelos a Valera mañana en
la mañana". Esto sería así en un mundo ideal, pero en nuestro mundo los
resultados serían un completo desastre, nuestro buscador nos daría
páginas de agencias de viajes, sitios de aerolíneas, blogs acerca de
"Valera" como localidad del Estado Trujillo, sitios turísticos en Valera
e incluso noticias sobre personas con el apellido "Valera", además de
recursos que contienen la palabra clave "mañana", estos resultados no
son exactos y, por si solos, no satisfacen las necesidades de
información del usuario, nuestro desafortunado viajero, tendrá que ir de
resultado en resultado extrayendo manualmente la información que resulte
relevante a su pregunta.

En la Web 3.0, un buscador con capacidad semántica, será capaz de
detectar automáticamente la ubicación del usuario (en mi caso, Caracas),
por lo que el "lugar de orígen" no tendría que ser suministrado, además,
el buscador "entendería" que el usuario desea aerolíneas que cubran la
ruta Caracas - Valera y calcularía el "mañana" en función de la fecha
actual del sistema, en decir, en función de un "hoy" e interpretaría la
segunda ocurrencia de "mañana" como un momento determinado del día, todo
esto sólo con un click!. ¿Cómo es esto posible?, pues dotando la web de
mayor significado en los innumerables recursos que pone a nuestra
disposición, dotando a nuestra Web 2.0 de una mayor semántica, de manera
que los resultados no se procesarían en base a entradas y salidas de
datos, sino en base al contexto y significado de la consulta realizada,
todo esto apoyándose en una infraestructura de metadatos. Suena simple
¿verdad?. Pero, ¿cómo construimos esa infraestructura?, en los próximos
artículos me dedicaré explorar más profundamente las posibilidades de la
Web Semántica (que, algunas, parecen sacadas de una película de Ciencia
Ficción) y a explicar a mayor detalle varios conceptos que hacen vida
dentro del marco de la Web Semántica, así como también ilustrar de
manera práctica, mediante tutoriales, el uso de las herramientas y
tecnologías necesarias para dar vida a la Web Semántica o, lo que es lo
mismo, dar Semántica a nuestra Web en vida, siempre dejando claro que no
se trata de Inteligencia Artificial, sino de dar a las máquinas la
capacidad de resolver problemas bien definidos, con operaciones bien
definidas y sobre datos bien definidos.

### Lecturas Recomendadas 

Cobo, Cristóbal y Pardo, Hugo. (2007) Planeta
2.0: Inteligencia Colectiva o Medios Fast Food. México DF: Grup de
Recerca d’Interaccions e Digitals.
