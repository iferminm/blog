Title: Algo hemos hecho mal en Venezuela, ¿qué?
Date: 2014-10-12
Author: Israel Fermín Montilla
Tags: reflexion, personal

Voy ya para cinco meses viviendo fuera del país y empiezo a ver las
cosas de otra manera, quizás estando en Venezuela todo estaba tan
caótico que me impedía pensar "fuera de la caja", quizás llegó el
momento en el que dejó de importarme y me enfoqué en mis asuntos y en
ayudar en lo que estuviera en mis manos, lo que sí estoy seguro es que
desde afuera y conociendo otra cultura veo las cosas desde otra
perspectiva.

Quizás uno ya ve todo como normal, pero, ciertamente no lo es, me
pregunto, ¿por qué?, ¿qué está mal en latinoamética?, ¿seremos los
latinos?, ¿será verdad que la culpa la tienen los españoles por mandar
puros vagos, borrachos y piratas a conquistarnos?, no lo se, lo que sí
se es que tengo demasiadas preguntas similares y dudo que pueda
responderlas todas, pero bueno, acá vamos.

Creo que **todos** sabemos que [Venezuela tiene una de las reservas
petroleras más grandes del
mundo](http://en.wikipedia.org/wiki/List_of_countries_by_proven_oil_reserves){.reference
.external}, ¿no?, no es secreto para nadie, estamos acampando, nos
dieron ganas de ir al baño, cavamos una letrina y *BOOM*, conseguimos
petróleo mágicamente, bueno, quizás exagero, pero estamos de primeros en
la lista, seguidos de cerca por Arabia Saudita.

No compararé Venezuela con Arabia Saudita por varias razones

1.  Están de segundos en el listado de países con más reservas de
    petróleo en el mundo, muy cerca de Venezuela.
2.  Son una de las cuatro monarquías absolutas en la actualidad, con
    todos los avances en esta era moderna, considero que estan atrasados
    en lo político y lo social, o eso es lo que mi mente occidental
    piensa.
3.  Hay muchas cosas básicas que no están permitidas o están realmente
    censuradas.

Voy a comparar con los Emiratos Árabes Unidos (UAE) pues es una
experiencia que puedo contar de primera mano pues, a la fecha, llevo
casi 5 meses viviendo en Dubai.

Empecemos por las reservas petroleras, Venezuela es el primero, UAE el
séptimo, es decir, estamos, por mucho, encima de UAE en cuanto a
reservas de petróleo.

El petróleo, sigue siendo el principal combustible para **casi todo**, y
sus derivados también están presentes en casi todo lo que vemos, así que
parece ser un buen negocio.

Venezuela inició su carrera independentista del imperio español el 19 de
abril 1810, cuando se llevó a cabo el primer referendum de nuestra
historia, aquel en el que el Capitán General Vicente Emparam va al
balcón a preguntarle al pueblo si quieren que él siga gobernando, el
Padre José Cortés de Madariaga, chileno de nacimiento, agitaba la mano
por detrás del Capitán General, algunos dicen que estaba espantando una
mosca, otros dicen que le hacía una señal negativa al pueblo, lo cierto
es que el pueblo gritó un rotundo "NO!" y así empezó nuestra agitada
carrera hacia la independencia, la cual sellamos el 24 de junio de 1821
con la victoria de nuestro ejército patriota, Forjador de
Libertades\[1\], en Carabobo. O eso es lo que dicen los textos de
historia que estudié en el liceo.

![La famosa escena del 19 de
abril](https://dl.dropboxusercontent.com/s/0rqe0kwn4yxeubp/emparan.jpg)

Por su parte, UAE logró su independencia del Reino Unido en 1971 no he
leído mucho de su historia aún, pero lo que sí se es que hasta
principios de la década de los 90, [esto era un
desierto](http://www.forocoches.com/foro/showthread.php?t=1243603){.reference
.external}, y hablo de Dubai, en 13 años levantaron rascacielos, para
2008 o 2009, ya tenían incluso islas artificiales, hoy día el edificio
más alto del mundo queda en Dubai ([Burj
Khalifa](http://es.wikipedia.org/wiki/Burj_Khalifa){.reference
.external}), los edificios 100% resicenciales más altos del mundo
también ([23 marina](http://en.wikipedia.org/wiki/23_Marina){.reference
.external} y [Princess
Tower](http://en.wikipedia.org/wiki/Princess_Tower){.reference
.external}) e incluso uno de los hoteles más lujosos del mundo,
contruido en una isla artificial ([Burj al
Arab](http://es.wikipedia.org/wiki/Burj_Al_Arab){.reference .external}).

![Dubai en
1990](https://dl.dropboxusercontent.com/s/1uhz4nqv925vc0e/dubai-desierto.jpg)
![Misma calle en
2003](https://dl.dropboxusercontent.com/s/1unk24ambjrj6tt/dubai2003.jpg)

Ahora bien, todo esto fue a punta de petróleo, acá no hay bellezas ni
muchos recursos naturales, sí, hay playas muy bonitas, pero ¿montañas?,
¿nieve?, ¿picos?, ¿sabana?, pues no, acá donde no hay rascacielos y
edificios enormes, hay arena, no estoy seguro cómo obtenemos agua
potable acá porque según me han dicho, son contadas las veces que llueve
durante el año, así que no creo que haya embalses naturales como en
Venezuela, ¿turismo? hoy claro que sí, hay maravillas arquitectónicas y
de ingeniería, invirtieron sus ganancias petroleras en infraestructura,
servicios y crecimiento.

En Venezuela, ¿qué hemos hecho con el petróleo?, desde que estudiaba
primaria, mi maestra de *sociales* siempre decía "Venezuela es un país
monoproductor de petróleo" y eso mismo fue repetido por distintos
profesores hasta que salí de bachillerato y luego durante la
Universidad, en una que otra materia social (recuerden que estudié
Ingeniería), también, el mismo cuento y las mismas palabras. Todo, lo
que se hace en Venezuela, es también *a punta de petróleo*, entonces,
por qué hay rascacielos, islas artificiales y autopistas de 16 canales
en Dubai, mientras que en Venezuela aún hay que pasar por carreteras de
tierra para llagar a algunos pueblos en los que llega señal celular de
Movistar, pero no una línea telefónica de tierra de CANTV, y ni hablemos
de internet de banda ancha. Mientras en un país se levantan rascacielos,
en el otro tardan **años** para reparar una torre quemada de Parque
Central.

![Parque central en
2004](https://dl.dropboxusercontent.com/s/542kzmo8bzzqox1/parque-central-quemado.jpg)

Y eso que Venezuela tiene más tiempo en el mercado petrolero que UAE,
Venezuela es uno de los países fundadores de la OPEP (Organización de
Paises Exportadores de Petróleo) en 1960, mientras que los Emiratos
Árabes, ingresaron en 1967, y en un país las escuelas parecen
universidades americanas (sí, las escuelas, colegios, liceos), y en el
otro el sistema de educación pública y gratuita a veces sufre para
iniciar el año escolar por falta de pupitres, goteras o fugas de agua en
los baños.

No comprendo, además, cómo en un país es normal ver tasas de homicidios
de 60 o 70 por semana, mientras que en el otro le roban el bolso a un
amigo en la playa (ayer) con la ropa, el celular y las llaves del
apartamento y **todo el mundo** está impresionado por la noticia.

Cómo UAE teniendo mucho menos petróleo que Venezuela, está en el lugar
28 entre las [economías más estables del
mundo](http://www.heritage.org/index/ranking){.reference .external},
mientras que Venezuela aparece de cuarto si contamos de forma regresiva.

Esas son todas las preguntas que me hago, cualquiera podría decirme "el
desarrollo de un país no se mide por la cantidad de rascacielos que
tenga", es cierto, vamos a verlo por calidad de vida, en Venezuela,
ejerciendo mi profesión, mudarme de casa de mis padres, así fuera a una
habitación, era un sueño. Acá, puedo alquilar algo decente tranquilo,
hacer mercado y darme uno que otro gusto de vez en cuando, y en una
posición "de menor jerarquía" en la cadena alimenticia, en Caracas, era
*Lead developer / CTO* de un startup de telefonía en la nube, acá soy
*Jr. Software Engineer*.

No vamos a verlo por cantidad de rascacielos, vamos a verlo por cantidad
de recursos naturales, en Venezuela tenemos Petróleo, Gas, Carbón,
Aluminio (Bauxita, creo), Oro, Níquel, Cobre, Hierro, incluso creo que
Diamantes y, además Energía Hidroeléctrica según recuerdo de las clases
de Geografía del Prof. Tito Calderón.

No vamos a verlo en cantidad de rascacielos, vamos a verlo en
posibilidades de hacer turismo, y digo turismo natural: tenemos la Isla
de Margarita, donde tengo entendido que se puede hacer surf, wind surf y
kite surf y además, las playas más impresionantes que he visto se
encuentran contenidas en los más de 4mil Kilómetros de costa venezolana,
los ríos en la sabana, donde se puede hacer Kayak extremo, la Gran
Sabana, los asentamientos indígenas en Amazonas y Delta Amacuro, la
caída de agua más alta del mundo, el Salto Ángel, la Cueva del Guácharo,
tenemos sabana, desierto, playa y montaña, todo en un mismo país de poco
menás de 900 mil kilómetros cuadrados, ¿no tan grande eh? y además
suelos fértiles por todos lados.

Cómo es posible que un país, mi país, teniendo todo eso esté en las
condiciones que está, que toda la población joven y profesional está
emigrando, literalmente *dejando el pelero*. Dicen que nadie es profeta
en su tierra, mi teoría es que Arturo Uslar Pietri es el hombre que ha
estado más claro en la vida cuando escribió su artículo "Sembrar el
Petróleo", mi teoría es que en algún momento oculto de la historia, se
vino de paseo al medio oriente y, como el suelo es pura arena y es muy
difícil hacer crecer plantas, hicieron crecer edificios.

¿Alguien me puede explicar?

![dafuq?](https://dl.dropboxusercontent.com/s/bkk53ile2u1jlrs/jackie-chan-meme.jpg)


\[1\] Hoy, Follador de libertades
