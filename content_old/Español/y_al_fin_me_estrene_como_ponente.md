Title: Y al fin!, me estrené como ponente
Date: 2013-08-10
Author: Israel Fermín Montilla
Tags: personal

Bueno, esto ya es tema viejo, pero no había tenido tiempo de escribir al
respecto. Desde que organicé el *PyConVE2012* había tenido la idea de
probar cómo me iba dando alguna ponencia o algún tutorial, ya en la
Universidad era preparador y miembro del *Sun Team University*, una de
las iniciativas académicas de *Sun Microsystems* que *Oracle* mató luego
de la compra. En el *Sun Team* daba charlas y también como preparador
hablaba en público, pero es muy diferente el ámbito académico de hablar
en una conferencia ante otros especialistas quizás con más experiencia o
mejor preparados que yo.

Desafortunadamente, en el *PyCon* sabía lo que me esperaba pues éramos
pocos organizando e iba a tener que estar corriendo de un lado a otro de
la sede (la UCAB) resolviendo problemas de última hora, así que decidí
no postular ninguna charla.

Este año, mi buen amigo [Jin
Kadaba](http://twitter.com/kadaba) estaba
organizando el [FLISOL 2013](http://flisol.org.ve),
y me invitó a dar una charla. Como es una conferencia de
Software Libre y se acepta cualquier tema relacionado, decidí iniciarme
con una charla no-técnica, hablé sobre Comunidades de Software Libre y
mi experiencia en *PyVE* y organizando el *PyCon*. Al parecer a la gente
le gustó.

Luego de un par de meses, Mimia Lo, una buena amiga de la Universidad,
estaba de nuevo organizando las
[JOINCIC](http://joincic.com.ve) y me invitó a
proponer alguna charla, confieso que tengo un poco de miedo a hablar en
público, así que decidí no pararme en la tarima del Aula Magna, sino más
bien dar un taller práctico, propuse un par de mesas de trabajo a ver si
aprobaban al menos una, para mi sorpresa me pidieron dar las dos :-) y
estaba muy contento.

Tanto el FLISOL como las JOINCIC fueron muy gratas experiencias para mi,
tuve la oportunidad de compartir y conocer a otras personas entusiastas
del Software Libre en el FLISOL y de compartir con viejos amigos de la
Universidad en las JOINCIC, sin mencionar que pude dar a conocer algo de
Python en ambos eventos.

Acá les dejo los slides de las tres presentaciones, espero sean de su
agrado:

**FLISOL**

*Comunidad: orden dentro del caos, caos dentro del orden*
<http://www.slideshare.net/iferminm/comunidad-caos-dentro-del-orden-orden-dentro-del-caos>

**JOINCIC**

*Testing: la etapa olvidada*
<http://www.slideshare.net/iferminm/testing-etapa-olvidada>

*Iniciación a las artes marciales con Python*
<http://www.slideshare.net/iferminm/iniciacin-a-las-artes-marciales-con-python>
