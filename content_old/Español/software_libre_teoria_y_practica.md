Title: Software Libre: Teoría y Práctica
Date: 2011-07-02
Author: Israel Fermín Montilla
Tags: software libre

Muchas veces, hablando de computación y tecnología, mis amigos y
familiares me preguntan qué tengo yo en contra de Windows
específicamente, y siempre respondo "nada", y es verdad, no tengo nada
en contra de Windows o Microsoft, sino del esquema de negocios basado en
Software Privativo, y esto suena medio comunistoide pero no me importa
porque quienes me conocen saben que no lo soy.

Y es que la razón por la que los detractores del software libre
defienden y promueven al software privativo, aún sin trabajar para
alguna compañía que lo produzca, son realmente absurdas. La principal de
ellas, y la que más me molesta en ocasiones, es el típico mito:
*¿software libre?, ¿gratis?, yo quiero hacer plata con software y con
algo gratis no se puede*, si, creanlo o no, me lo han dicho bastante.

Para comenzar, permítanme decirles que quien piense y esté seguro de que
Software Libre es igual a Software Gratis, está equivocado. Ciertamente,
gran cantidad de Software Libre está disponible en la web de manera
gratuita, pero también lo está una gran cantidad de Software Privativo,
a los programas que son liberados de manera gratuita, se les conoce como
freeware. Entonces, si Libre no es igual a  Gratis, entonces ¿qué es Software
Libre?. Software Libre, en su definición más purista es aquel que cumple
con las cuatro libertades:

-   **Libertad 0:**libertad de usar el programa para
    cualquier propósito. Realmente no importa si vas a utilizarlo en tu
    casa, en tu oficina, vas a vender una instalación, puedes utilizarlo
    sin ningún problema.
-   **Libertad 1:**libertad de estudiar como funciona el programa,
    modificarlo y adaptarlo a tus necesidades. Si vas a utilizar un
    software, y más aún si pagas por él, debes poder conocerlo a fondo y
    si eres usuario técnico, deberías poderlo modificar y adaptarlo a lo
    que realmente quieres que haga, si no puedes adaptarlo a tus
    necesidades entonces mejor no usarlo.
-   **Libertad 2**: libertad de hacer copias del programa y
    distribuirlas a tus amigos. Si algo es bueno, obviamente quieres que
    otras personas lo usen y se beneficien de él.
-   **Libertad 3:** libertad de hacer públicas las modificaciones que
    hagas para que toda la comunidad se beneficie. Si eres usuario
    técnico de un software del que te beneficias en tu día a día
    personal o laboral, te conviene que mejore para incrementar
    tu productividad.

Por ello, si descargo de internet un programa gratis y no tengo acceso
al código fuente, no estamos hablando de Software Libre, aún cuando sea
de distribución gratuita. Entonces, si desarrollas software libre y
vendes tus aplicaciones, debes, al menos, incluir el código fuente y no
restringir su uso. Entonces, ¿cómo se hace dinero con software libre?,
no es el programa, sino el soporte que le das, cualquiera puede
desarrollar "el programa del siglo", pero si el soporte y la
documentación no sirven, entonces ¿quién querría usarlo?. Cualquier
persona puede descargarlo, estudiarlo y utilizarlo si lo desea, pero
para configuraciones más avanzadas (igual que con el software privativo)
tendrá que llamar a un especialista. De la misma manera si se desea una
modificación sobre el programa original, si tienes el tiempo y el
personal para hacerlo, puedes descargar las fuentes, estudiarlo y
modificarlo, sino, igual tendrás que llamar a un especialista que ya
haya hecho este trabajo.

Si trabajas con software libre, no vendes un programa, vendes una
solución completa con un acuerdo de soporte, ahorrándole al cliente y a
tu compañía los costes de licenciamiento. Otro de los mitos urbanos
sobre el software libre es que *si no tiene una súper compañía por
detrás, no puede ser bueno*. Históricamente, existen muchas compañías
que desarrollan Software Libre, por ejemplo IBM y Novell son dos de las
que más han contribuido con el kernel de Linux, si bien es cierto que el
software libre se desarrolla de manera comunitaria, muchas de las
empresas que venden soluciones basadas en software libre, tienen un
staff de programadores dedicados a liberar código fuente a la comunidad,
entonces, no es *una súper compañía*, son muchas grandes, pequeñas y
mediadas empresas dedicadas al desarrollo de la herramienta alrededor
del mundo.

Finalmente, el más grande de los mitos, ya un poco más de usuario final:
*es más difícil de usar*, esto es totalmente falso, pongamos el ejemplo
típico: Windows vs Linux, nadie nació sabiendo utilizar Windows, sin
embargo, todos lo usan porque es lo que viene preinstalado en la mayoría
de las computadoras de marca (en nuestro país, lo venden "como tu
película" en los clones que usualmente uno compra), en algún momento de
nuestras vidas, tuvimos que haber invertido unos días aprendiendo, poco
a poco, a utilizarlo. Exactamente lo mismo ocurre con Linux, si bien
existen distros para usuarios técnicos (como Gentoo, Slackware y
CentOS), también existen muchas enfocadas al usuario final (como la
familia \*buntu, Fedora y Simplis), simplemente es cuestión de
decidirse, probarlo y usarlo sin miedo.
