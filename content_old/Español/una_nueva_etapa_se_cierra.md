Title: Una nueva etapa se cierra
Date: 2012-06-23
Author: Israel Fermín Montilla
Tags: carrera, personal

He tenido un poco abandonado el blog debido a mi trabajo especial de
grado (TEG) y a mi nuevo trabajo en Zava, pero poco a poco he ido
adelantando cosas y hoy conseguí algo de tiempo para escribir las
últimas cosas sobre las que he estado reflexionando las últimas semanas
(o meses).

Bien, tengo la satisfacción personal de haber desarrollado una buena
tesis, obtuve una calificación de 19 puntos con mención honorífica, al
jurado pareció haberle gustado mucho el trabajo que hice, mi tutor,
el Prof. Wilmer Pereira, también quedó muy satisfecho con el resultado,
igual que mi colaborador/asesor/amigo, el Prof. Carlos Pérez Díaz,
digamos que estoy cerrando otra etapa de mi vida con broche de oro, a
pesar de tardar más de 5 años en culminar mis estudios de Ingeniería en
Informática, estoy muy feliz con todo lo que aprendí y compartí con mis
profesores, algunos, verdaderos maestros, con mis compañeros y amigos,
especialmente
con Ronald Oribio, Khaterine Castellano, Viviana Trujillo, Gerardo Barcia y Jhonatan Trujillo.
También con todos los que les dí clases en mis días de preparador y
asistente docente: Ayleen Posadas, Karen Barreto, Héctor Sam,
Juan Perozo, Alfredo Nava, Oswaldo Bracho, muchísima gente hizo mi paso
por la universidad, una etapa muy especial, sobre todo mi
novia, Giselle Bracamonte, en mi último semestre, puede que quien esté
leyendo esto no conozca a quienes menciono, pero realmente no me
importa.

Ahora que cierro este ciclo, hay muchas cosas sobre las que reflexionar,
sobre todo ahora cuando tengo ya un poco más de un año trabajando,
desarrollando software, programando, diseñando/arquitectando soluciones,
investigando y, a veces, sufriendo por la falta de sueño, pero eso será
en otro artículo, en este me gustaría reconocer a aquellos profesores
que se ganaron mi respeto dentro del aula y tuve el gusto de ganarme su
amistad fuera de ella. Salimos de la universidad creyendo que lo sabemos
todo y, en realidad, lo sabemos, pero al mismo tiempo
no. Permítanme explicarme, conocemos la teoría, sabemos lo que es una
clase, un objeto, sabemos lo que son pruebas unitarias, podemos diseñar
e implementar un modelo de datos relacional, podemos, literalmente,
"echar código", pero la habilidad de "programar", se va adquiriendo con
el tiempo, de la misma manera que la habilidad de "entonar" un servidor
o una red. Acabados de salir de la universidad, únicamente, tenemos las
bases, queda de nuestra parte terminar de construir el edificio. Para
mi, los profesores quienes, realmente, me ayudaron a afianzar esas bases
fueron:

-   **Lúcia Cardoso** (en Sistemas de Base de Datos I y II), con ella
    aprendí que ser un Ingeniero en Informática, no es sólo programar,
    esas líneas y secuencias de instrucciones, normalmente, tienen el
    objetivo de satisfacer las necesidades de información de "alguien",
    por ello, debemos preocuparnos por cada aspecto de nuestra solución,
    desde el modelo de datos hasta la manera de presentarlos para que se
    conviertan en información. Un buen modelo de datos da un buen
    soporte a la información que manejará nuestro programa y nos
    facilitará la vida en capas superiores. Es además, una profesora muy
    exigente y tiene fama en los pasillos de ser uno de los filtros de
    la carrera, una fama muy bien ganada. Hoy, agradezco enormemente su
    nivel de exigencia.
-   **Carlos Barroeta** (en Ingeniería y Desarrollo del Software), con
    él aprendí principios vitales para cualquier programador:
    especialmente "No reinventar la rueda", si hay algo ya probado y
    funciona, ¿por qué lo usarlo y ahorrarnos trabajo?. Temas como
    Patrones de Diseño, Programación Orientada a Objetos, Pruebas de
    Software, Separación por Capas y Metodologías Ágiles fueron el día a
    día en estas asignaturas. Es famoso en la Escuela por ser un
    entusiasta de las nuevas tecnologías y los Stack-Frameworks que
    facilitan la vida del desarrollador, hoy agradezco que haya puesto
    tanto empeño en explicar esos conceptos y en asegurarse de que los
    entendiera (a través de proyectos prácticos y complejas preguntas en
    los exámenes), de no haber sido así, hoy probablemente sería un
    programador terrible y sin ganas de superarse.
-   **Darío León **(en Sistemas de Operación), aprendí cómo funciona a
    bajo nivel un sistema operativo. Los conceptos de Cambio de
    Contexto, Planificación de Procesos, qué hace el Kernel y cuáles son
    los componentes y servicios de un Sistema Operativo, aprendí de
    administración básica de sistemas Un\*x y, creo que una de las cosas
    que hoy me quedan, aprendí a utilizar la herramienta que uso hoy
    para escribir cada línea de código que produzco: el editor vim.
-   **Rodolfo Campos **(en Sistemas Distribuidos), aprendí varios
    conceptos macro en cuanto a coordinación de sistemas paralelos
    (como un clúster) y distribuidos (como un grid): algoritmos de
    reloj, sincronización, elección del líder, protocolos de
    comunicación, redundancia y tolerancia a fallos. Fue una de las
    materias que más disfruté de la carrera, es realmente increíble cómo
    puede uno, como ingeniero, hacer posible que dos dispositivos en
    lugares geográficamente distintos puedan coordinar acciones para
    lograr una tarea en común y, además, que todo sea transparente para
    el usuario.
-   **Wilmer Pereira**, aunque nunca me dio clases formalmente, fue mi
    tutor de tesis y mentor en el área de investigación, siempre lo he
    considerado un modelo a seguir por todo el conocimiento que tiene y
    por su humildad y sencillez a la hora de hablar con un alumno o con
    cualquier otra persona, es realmente una persona brillante que a
    veces pareciera vivir en su propio mundo.

Sus lecciones académicas y personales son muy valiosas, no puedo más que
decirles: MUCHAS GRACIAS!, espero llegar a ser al menos la mitad de
buenos de lo que ustedes son.
