Title: Mi experiencia en el PyConVE 2012 y otros cuentos
Date: 2012-11-18
Author: Israel Fermín Montilla
Tags: pycon, pyve, python

Bueno, ya ha pasado una semana y alguito desde que terminó la primera
Conferencia del Lenguaje Python de Venezuela (PyConVE), he tenido algo
de tiempo para reflexionar y pensar acerca de varias cosas que me
ocurrieron antes del evento, es decir, mientras se estaba organizando,
durante el evento y después del mismo, es decir, los días siguientes a
que la conferencia finalizó.

## ¿Cómo comenzó?

Creo que es justo *echar el cuento* de cómo surgió la idea de organizar
un PyCon en Venezuela, algo que para muchos, dentro de la comunidad
incluso, resultaba una locura hace un par de años.

Todo comenzó por allá en febrero de 2011, yo empezaba mis pasantías
cortas de la Universidad en [Vauxoo](http://vauxoo.com),
me iniciaba en Python con aquel trabajo, era toda una nueva
manera de programar y me encantaba, por referencia de Nhomar Hernández
terminé metido en la lista python-caracas y, posteriormente, en la de
Python Venezuela en coactivate. Recuerdo algunos mensajes en la lista de
python-caracas se estaba hablando de que sería una buena idea organizar
un *Día Python* para vernos las caras, yo era joven e ingenuo, me ofrecí
para organizarlo y en marzo de 2011 tuvimos nuestro primer *PyDay
Caracas*, en el auditorio de la biblioteca de la Universidad Católica
Andrés Bello.

Luego de ese primer *PyDay*, que se repitió en Mérida unos días después,
Francisco Palm decía por la lista que se debería aprovechar el impulso
para organizar algo más grande: un *PyCon*. La idea, por esos días, no
caló mucho entre la gente de la comunidad, las cosas se fueron enfriando
poco a poco y con el tiempo, sólo había uno que otro mensaje ocasional
en la lista y nos veíamos una vez cada alineación planetaria.

Luego, por allá en diciembre de 2012, conversando con Francisco Palm, la
idea de un *PyCon* no me pareció tan descabellada, había posibles
patrocinantes, la sede podría ser la UCAB, se asomaba la idea de invitar
ponentes internacionales, yo por mi parte, acababa de renunciar en
[Vauxoo](http://vauxoo.com) para dedicarme a mi
tesis de grado, entonces, también trabajaba en mi tesis, seguía siendo
joven e ingenuo y acepté lanzarme de frente con la organización del
evento.

## El comienzo

Justo empezando, nos dividimos un poco las tareas, decidimos que el
evento sería en Caracas y que la sede sería la Universidad Católica
Andrés Bello, al ser *casi egresado* de allí, ya se cómo se mueven las
cosas dentro, conozco a varias autoridades y se también con quién hablar
o dónde dirigirme.

Había algo que para mi resultaba vital, tener apoyo de mi Escuela, la
Escuela de Ingeniería Informática de la UCAB. Lamentablemente, aunque el
Prof. Ricardo Casanova tenía instrucciones de apoyarme en lo que
pidiera, no hizo más que decirme que "los auditorios y los laboratorios
no pueden reservarse con tanta antelación", de resto, resolví
escribiéndole a las unidades funcionales correspondientes y reservando
laboratorios y el auditorio "con mucha antelación". Me hubiera gustado
contar con un mayor apoyo por parte de mi Escuela, pero supongo que
aquello de "nadie es profeta en su tierra" es cierto.

Una vez asegurados los espacios para alojar el evento, me dediqué a
buscar ponentes, ya había unos nacionales más que confirmados a quienes
no había siquiera que preguntarles: Francisco Palm, Nhomar Hernández,
Carlos Gustavo Ruíz, Carlos Zager y Juan Hernández, pero para atraer más
personas al evento, hacían falta ponentes internacionales, me dediqué a
escribirle a **Guido Van Rossum** y a **Wesley Chun** a ver si estarían
interesados en participar, eso sí, teniendo presente que tocar la puerta
no es entrar.

De parte de Guido, obtuve una respuesta negativa, él dice que prefiere
no viajar porque es malo para su familia y es totalmente entendible, ya
tiene su agenda y va al *PyConUS* y al *EuroPython* casi exclusivamente.

Por su parte Wesley también me dijo que no, sin embargo me facilitó una
larga lista de contactos, me puso en contacto con José Montes de Oca
(venezolano en Google) y me pidió que lo mantuviera al tanto del evento.
Gracias a Wesley Chun, pude contactar con Facundo Batista y Érico
Andrei, los dos ponentes **internacionales** del primer *PyConVE*, la
invitación incluía **pasaje y hospedaje** en Caracas, ambos aceptaron y
yo, en ese momento, **no** tenía patrocinantes.

## Tomando forma y sintiendo la presión

Bien, en enero ya tenía cuatro cosas: una fecha para el *PyConVE*, dos
invitados internacionales **confirmados** (Facundo y Érico), un invitado
internacional **por confirmar** (José Montes de Oca) y la preocupación
de conseguir el dinero para cumplir la promesa del pasaje y el
alojamiento en Caracas para los invitados, bastante ¿no?

Empecé a escribir a mediados de enero a la Escuela de Ingeniería
Informática (EII) para pedirles **apoyo** en la logística interna,
especialmente la reserva de los auditorios, la Prof. Susana García,
directora de la EII, designó al Prof. Ricardo Casanova para que me
ayudara en lo que fuera necesario. Procedí entonces a contactarlo para
la reserva del auditorio y los laboratorios, su respuesta fue, en
resumen, "no puedes reservar *con tanta antelación* en este momento
estoy muy ocupado y no puedo", este personaje y yo tenemos en el pasado
un problema originado por una discusión *de esos temas religiosos* Mac
vs Linux del que, aparentemente, no ha podido sobreponerse.

Dada la negativa de apoyo o, más bien, intento de retrasarme las cosas,
decidí contactar directamente a la Dirección del Cultura y a la
Dirección de Tecnologías de Información (DTI) para reservar los
espacios. Como era de esperarse, todo fluyó con normalidad y ya a
finales de enero tenía algo mas: el Auditorio Hermano Lanz y los
laboratorios A613 y A553 de la UCAB **reservados** para albergar el
*PyConVE*

Las cosas se durmieron un poco hasta finales de febrero, que los
organizadores de *PyConAR* lanzaron su página web con el llamado a
charlas, inscripciones, información del evento, invitados
internacionales y demás información del evento. Acá no teníamos siquiera
un *En Construcción* y, en pocas palabras, **me asusté** (sintiendo la
presión). Corrí a la lista *Conferences* de *python.org* para pedir
ayuda con el dominio oficial *ve.pycon.org* y, mientras, compré el
dominio *pyconve.com* a través de *GoDaddy*. Aprovecho acá para
agradecerle a M.A. Lemburg por su ayuda configurando el dominio.

Ya que las cosas no fluían con la velocidad que quería con la página
web, pedí ayuda en la oficina, en ese momento trabajaba en
[Zava](http://zava.com.ve), nos dedicamos 4
desarrolladores a sacar la página y en una tarde ya teníamos algo visual
y semi-funcional, al final de la semana ya teníamos una página bastante
completa y *apta para el público*, la hospedamos en el VPS de la empresa
y listo. Posteriormente la empresa me dejaría tiempo para atender bugs y
añadir características nuevas, esto fue en abril.

## Empezando a tranquilizarme

Conversando con Francisco, me di cuenta de que las cosas no iban tan
atrasadas como pensé, él había conseguido patrocinio para afiches, agua
y almuerzos para los organizadores y ponentes durante el evento, yo
había conseguido ponentes internacionales, sede y página web así que lo
demás, iría llegando poco a poco.

Todavía tenía una preocupación más: **LOS PASAJES**, me dispuse a
escribirle a Nhomar Hernández a ver si desde Vauxoo o algún
cliente/socio de negocios podrían patrocinar los pasajes. La respuesta
fue afirmativa y, ya a mediados de septiembre, estaba enviándole a Érico
y a Facundo sus tickets electrónicos, en cuanto a José, el ponente de
Google, tenía tiempo sin noticias hasta que confirmó, pero para cuando
confirmó los pasajes habían subido de precio de manera astronómica y,
difícilmente, un patrocinante iba a querer pagarlo, así que llegamos al
acuerdo de que sería por videoconferencia a través de un *Google
HangOut*, escribí al DTI para que realizaran las configuraciones
pertinentes.

Mientras tanto, teníamos unas 35 ponencias inscritas y alrededor de 150
personas registradas para asistir al evento, esto me tenía súper
contento, sinceramente no esperaba tanta receptividad. Francisco
consiguió organizar un evento en el CIDA en Mérida y logró que un
patrocinante pagara pasaje Caracas - Mérida para que Facundo participara
en ese evento, el *PyTatuy*. Todo parecía ir viento en popa.

## Imprevistos, imprevistos, IMPREVISTOS!!!

Bueno, nada es perfecto, los imprevistos en este tipo de cosas están a
la orden del día, recordemos que
[Murphy](http://es.wikipedia.org/wiki/Ley_de_Murphy)
existe. Previendo que, quizás, no llegara el agua a tiempo el
primer día. Facundo decidió venir con su familia, ahora debía buscarle
una habitación en un Hotel cerca de la Universidad, tienen un hijo
pequeño, así que debía ser un buen hotel y, además, debía buscar un
patrocinante para eso, la habitación no se concretó sino hasta tres días
antes de que llegaran el patrocinante fue
[EchandoCódigo](http://echandocodigo.com) (Gracias
Osledy Bazó!!!).

En la madrugada del 27 de octubre recibo una llamada a mi celular, era
un número internacional y atendí. Del otro lado decían *"Ché ¿Israel?,
Facundo Batista aquí, estoy en el aeropuerto y no me dejan abordar
acá"*, había un problema con el nombre, para enterarse del cuento
completo, click
[acá](http://www.taniquetil.com.ar/plog/post/1/583).
Inmediatamente llamé a Nhomar a ver si estaba despierto (y
si no, pues que se despertara), estuvimos conversando un rato y luego
Facundo me informó que ya le habían liberado los boletos, pero que era
necesario corregir los boletos para el regreso.

El 27 de octubre en la noche, voy con Giselle, mi novia, al aeropuerto a
buscar a Facundo y familia, pero al llegar, me consigo únicamente a
Facundo, la familia se había quedado varada en Lima, les habían
cancelado el segundo tramo del viaje. Bueno, subimos a Caracas y lo dejé
en el Hotel para que pudiera descansar y al día siguiente ya se
resolvería lo del vuelo de la familia, afortunadamente llegaron en
domingo 28 al mediodía.

Francisco Palm había quedado en traer los afiches para pegarlos por la
universidad cuando viniera a Caracas con Facundo luego del *PyTatuy*,
pero los dejó en el carro y el carro lo dejó en Mérida (sorry!, tenía
que contarlo, no lo tomes a descarga, jajajajaja).

Con respecto a Érico, todo fluyó de maravilla, se quedó en mi casa,
comió arepas, aprendió a moverse en Metro, andaba solo por la ciudad!!!,
eso me preocupaba enormemente, pero luego entendí que él es muy
independiente cuando viaja.

Hubo cancelaciones tardías: Ernesto Crespo sufrió una caída que le
imposibilitó viajar (espero se encuentre bien ya), Roldan Vargas tampoco
podría venir a Caracas, Edwind Ocando iba a ser operado y tuvo que
cancelar también, Efraín Valles no pudo venir por razones laborales.
Hubo otras más, pero no recuerdo, afortunadamente varios miembros
saltaron al rescate, Gerardo Curiel cubrió varias charlas canceladas con
temas interesantísimos de desarrollo web y vim para pythonistas,
Leonardo Caballero cubrió una de las charlas de Plone que fueron
canceladas y Érico Andrei también dió una charla adicional muy orientada
al público estudiantil.

Bien, llegó el primer día del *PyConVE* y, como era de esperarse, fue un
desastre, había una cola insoportable en la Av. Páez, ergo, llegué tarde
con Érico, Gigi (bueno, Giselle, mi novia) ya había llegado y me ayudó a
ir organizando todo, ya estaban Sebastián Magrí y Leonardo Caballero en
la UCAB, Rafael Andara (del DTI) me había estado esperando para darme
los datos de configuración para poder realizar la videoconferencia con
José Montes de Oca, muchísimas gracias a Gigi y a los muchachos por ir
alistando todo, el evento arrancó casi una hora y media tarde (perdonen
todos el retraso).

Las aguas llegaron tarde (menos mal fui precavido), los almuerzos
también así que se convirtieron en la cena de varias personas
(incluyéndome).

Vale acotar que pasé **toda la semana** del PyCon enfermo del estómago y
sobreviviendo a base de Loperán y Alcaseltzer.

El último día del evento, el DTI no me abrió los laboratorios porque no
tenían la llave y la oficina donde están no abre los sábados (mal!) y,
supuestamente, hay una persona los sábados que tiene las llaves pero ese
día no fue (terrible!!), por lo que todos los talleres de ese día (o la
mayoría) hubo que darlos como charlas relámpago en el Auditorio (una
manera de resolver y no quedar tan mal).

## Conclusiones

Bueno, luego del **PyConVE**, aprendí muchísimas cosas en cuanto a
gestión de eventos y otras tantas de comunidad, sobre todo aprendí a
delegar, es imposible que una sola persona pueda hacerlo todo. Descubrí
que hay mucha gente valiosa dentro de la comunidad en quien se pueden
delegar cosas de manera muy confiable, [Jin
Kadaba](http://twitter.com/kadaba), [Luis Alberto
Santana](http://twitter.com/jackboot7), [Carlos
Gustavo Ruíz](http://twitter.com/carlosgr_arahat),
[Leonardo Caballero](http://twitter.com/macagua),
[Sebastián Magrí](http://twitter.com/sebasmagri),
[Nhomar Hernández](http://twitter.com/nhomar)
(pero por supuesto!), [Juan
Hernández](http://twitter.com/vladjanicek) (no
faltaba más), [Francisco Palm](http://twitter.com/mapologo).
y si olvido a alguien, pido disculpas.

Hay aún mucho trabajo por hacer, es necesaria una figura legal para
poder recibir donaciones en calidad de patrocinio de una manera más
formal, actualmente estamos en eso en la comunidad, creo que el PyCon,
aunque bien fue algo arriesgado, fue un catalizador y un activador para
muchas personas dentro de la comunidad, está en nosotros no dejar que la
ola muera y continuar trabajando para llevar adelante nuestra comunidad,
muchas personas de otros estados quieren organizar *PyDays*, personas
que antes no intervenían en la lista de la comunidad ahora son más
activas y sólo leí buenos comentarios del evento. Todo esto y, sobre
todo las dos primeras, con cosas que me hacen decir que **valió la
pena** el esfuerzo, y **lo volvería a hacer**. Perdonen si me extendí,
creo que este es el artículo más largo que he escrito en muchísimo
tiempo, pero eran demasiadas experiencias que quería compartir,
finalmente, muchísimas gracias a Facundo Batista y Érico Andrei por
participar, gracias a **todos** los ponentes nacionales por apartar esos
tres días en sus agendas y movilizarse a la UCAB para nuestro **primer**
*PyCon*, a mi novia por apoyarme en mis locuras y a todos quienes
asistieron de Caracas y, sobre todo, del interior del país, GRACIAS
TOTALES!.
