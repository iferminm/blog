Title: Reflexión del día que me fui del país
Date: 2014-11-21
Author: Israel Fermín Montilla
Tags: reflexión, personal

Bueno, no se si estoy intenso ya con el tema de las reflexiones pero
esta será la última al menos por un buen tiempo, lo que ocurre es que
hace poco conseguí un par de escritos que pensé que había perdido cuando
formateé la computadora acá en Dubai apenas llegué.

Esto lo escribí el Miércoles 7 de Mayo de 2014, justo el día que estaba
emigrando.


## Acá... lo que escribí

Siempre que viajo, tengo la mala costumbre de estar hasta la noche antes
del vuelo haciendo maletas, esta vez no fue la excepción.

Terminé de arreglar las cosas a las 2:00am con mi mamá, luego de eso me
puse a ordenar un poco el closset pues es probable que mi prometida se
quede en mi habitación hasta que, finalmente, nos casemos en diciembre.
Al final terminé acostándome a las 5:00am.

La mañana transcurrió sin mucha novedad, a las 11:00am ya estábamos
bajando al aeropuerto y ya a las 13:00 estaba chequeado y sentado
tomándome algo con mi familia, mi familia completa, la que me tocó (mi
papá, mi mamá y mi hermano) y la que yo tuve la suerte de elegir (mi
prometida y su familia).

Por recomendación de la aerolínea, debía entrar al área de embarque a
las 14:00, así que a esa hora nos dirigimos a la puerta que dirige a esa
zona, las lágrimas no se hicieron esperar, abrazos y besos para decirnos
“hasta luego” pues en diciembre estaré de vuelta.

El camino al aeropuerto estuvo lleno de reflexiones, en una de las
cuales planteé, ¿por qué tiene que ser así?, el ideal venezolano desde
que tengo uso de razón siempre ha sido graduarse e irse del país, sea
por trabajo o a estudiar un post grado, la cosa es, no debería ser así,
¿por qué no prosperar en tu propio país?, después de todo es tu casa,
¿no?. Pues la respuesta es muy sencilla, también, desde que tengo uso de
razón, la manera de sobresalir entre los demás, no es hacer las cosas
bien necesariamente sino ser *más vivo que el de al lado*, la
supervivencia del más vivo o, como se dice coloquialmente, *la viveza
criolla*, ojo, no digo que todo el mundo sea así, pero una gran mayoría
piensa así, entonces, el ideal de éxito pareciera ser “quédate y trata
de ser más vivo que los demás, o vete a un país donde todo el mundo sea
honesto y haz las cosas como deben ser”, es decir, no te comas la luz,
cruza la calle por el rayado, no botes basura en la calle, no orines en
la calle ni enseñes a tu hijo a hacerlo, entre muchas otras cosas.

La verdad es que la conversación estuvo muy amena, como siempre con mi
familia, la cuestión es que tomará bastante tiempo antes de sentarnos en
la misma mesa como hoy, a disfrutar de un buen café y hablar de
cualquier cosa, pasará algo de tiempo antes de que podamos volver a
abrazarnos como hoy mientras nos despedíamos, ahora que estoy
emprendiendo camino a otras latitudes, entiendo cuando mi mamá me decía
“algún día dirás, ¿por qué no abracé más a mis padres?”, le doy toda la
razón, nunca he sido muy cariñoso porque no me criaron así, pero justo
mientras escribo este post estoy sobrevolando el océano atlántico camino
a mi primera escala, el asiento a mi lado está vacío, como también
estará vacío el asiento frente a mi en unas horas cuando me siente a
almorzar y mañana cuando me siente a desayunar, y el día siguiente, y el
siguiente... darse cuenta de esto te pega un poco y despierta la
nostalgia y, lo peor, es que es la realidad de muchas personas que han
decidido experimentar “la triste alegría de emigrar”.

Creo que la misma situación del país me recordó las razones por las que
los jóvenes profesionales deciden irse a probar suerte en otras
naciones: justo pasando el chequeo de migración, escucho un llamado de
la aerolínea: “pasajero de Lufthansa Israel Fermín, favor presentarse en
la puerta de embarque número 12”, pensé que estaban abordando temprano
así que me apuré a la puerta de embarque, allí me informan que una de
mis maletas fue “seleccionada para revisión” y que debía bajar al área
de equipaje para abrirla, vaciarla y examinar su contenido. Allí, un
Guardia Nacional del Comando Antidrogas era el encargado de revisar mi
maleta, a pesar de que traté de buscarle conversación no era muy
hablador, pero tampoco me trató mal ni fue mal educado, simplemente hizo
su trabajo, de manera muy ordenada (de verdad, no es sarcasmo) vació el
contenido, revisó la manera y volvió a colocar todo en su lugar. Luego
de eso, le pregunto al encargado de equipaje que me acompañó si de
verdad consiguen mucho contrabando de esa manera, la respuesta fue
afirmativa, no sólo eso, sino que “es algo de todos los días”, de hecho,
hace una semana “aprehendieron a una familia completa”, sí, “el señor,
la esposa y dos niñas menores de edad”, según el señor la mayor tendría
más o menos 5 años.

Luego de la revisión de equipaje, el vuelo se retrazó unos minutos,
dándome tiempo de comer algo antes del vuelo para amortiguar, entre los
locales de comida, había una arepera que de verdad no se veía bien,
quería irme por lo más económico, y lo siguiente más económico era
Burger King, hago mi cola que cada vez se hacía más larga gracias a la
lentitud del servicio y de la única cajera que se multiplexaba en tiempo
entre la cocina y la caja. Finalmente, cuando voy a pedir, no hay
tocineta, no hay aros de cebolla, no hay... bueno, ya estamos
acostumbrados, al final pedí el combo de “Dos hamburguesas con queso”
por el cual me quedan debiendo 5 bolívares porque, de nuevo, no hay
cambio.

En la cola para retirar el pedido, de nuevo, haciéndose cada vez más
larga porque no repartían, informan que la máquina de refrescos está
dañada y que no hay refrescos y que no entregarían los pedidos hasta que
la arreglaran, allí solté mi tradicional “coño de la madre, qué peo”
(perdonen si leen y no esperan malas palabras en un blog), al final,
molesto, pido mis hamburguesas, mis papas o que me regresen mi plata,
así que me dan mis hamburguesas (frías... además de pequeñas), al
finalizar mi delicioso almuerzo (esto sí es sarcasmo), abordé el avión.

Lo que más me impresionó del episodio de Burger King, no fue el hecho de
que no hubiera nada, ni el mal servicio, sino que yo era el único que
estaba molesto y que estaba reclamando algo, los demás estaban
resignados a esperar quien sabe cuánto tiempo a que arreglaran la
máquina de refrescos, les dieran hamburguesas heladas y, seguramente, un
refresco que sabe a pura soda. Esa misma actitud de “cállate que sino te
van a joder” es la que nos tiene con el agua hasta el cuello y, también,
esa actitud de “si puedo jodo al que me cae mal, me mire o me hable feo”
es la que sigue echándole agua a la piscina en la que, si seguimos como
vamos, nos ahogaremos todos, mi pregunta es, ¿hasta cuándo vamos a vivir
en la mierda?, ¿hasta cuando vamos a aceptar un “no hay” por respuesta?.
