Title: Querido amigo usuario
Date: 2013-02-03
Author: Israel Fermín Montilla
Tags: carrera, frustraciones

by [Israel Fermín Montilla](author/israel-fermin-montilla.html) on Sun
03 Feb, 2013

En algunas carreras me llama muchísimo la atención el hecho de que todos
tus amigos, familiares, vecinos e incluso gente que acaba de conocerte
piensan que estudiaste para proveerles servicio gratis y que, además,
debes saberlo todo de todo o no sirves.

Si eres médico, nunca falta salga con cosas como *sabes que tengo un
dolorcito aquí en el brazo desde hace días, ¿qué será que puedo tomar?*,
o el típico *¿qué le puedo dar al chamo para que se le pase el
malestar?*, si eres fotógrafo, nunca falta el que te diga *chamo, tomame
una foto ahí pal' féijbu*. Si eres computista, la cosa es realmente
difícil.

Sí, soy **Ingeniero** en **Informática**, al igual que muchos otros
colegas por ahí, pero eso no me convierte automáticamente en tu soporte
técnico personal para repararte el *Güindows* cada ves que le entra un
virus, configurarte la impresora cada ves que compras una nueva o
colocarle clave a tu *wifi*. Claro que me gusta hablar de tecnología,
pero no conozco todos los modelos de *smartphone* ni mucho menos se cómo
arreglarte el *blackberry*.

Pero, sin lugar a dudas, los peores son los que llegan diciendo *mira,
necesito hackear esta cuenta de correo, ¿será que me puedes hacer el
favor?*. Bueno, yo necesito dinero, ¿robarías un banco por mi?, creo que
no. Estos últimos no aceptan un educado **no se** por respuesta, sino
que luego de eso te dicen *ay, pero tú deberías saber, ¿no les enseñan
en la universidad?*.

Querido amigo, familiar, vecino, en fin, usuarios todos, **no**, **no me
lo enseñan en la universidad**, vi muchísimas materias entre Cálculos,
Físicas, algunas Matemáticas Discretas, muchos Algoritmos y materias de
Programación, Sistemas Operativos y Redes de Computadoras, pero ninguna
de *Hackear Hotmail* I, II o II ni ninguna electiva de *Configuraciones
Avanzadas de Impresoras*, mucho menos *Detección y reparación de Virus
en Windows*, y ni hablar de *Gestión avanzada de equipos BlackBerry*.

Claro, me agrada ayudar a la gente, no tengo problema en ayudarte a
solventar tus necesidades tecnológicas de vez en cuando, pero **no
abuses**.

Con cariño Israel.
