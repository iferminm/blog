Title: Ahora soy lead developer y no tengo mucha idea: anécdotas y reflexión
Date: 2013-09-23
Author: Israel Fermín Montilla
Tags: reflexion, personal


Primero que nada, me gustaría pedir disculpas (o permiso xD) si cometo
algún error ortográfico en este artículo, se me olvidó pasar el
[aspell](http://aspell.net/){.reference .external}, el hecho es que,
para ponerlos un poco en contexto, vengo de un rato un poco etílico.

Bueno, para empezar, quienes me conocen y están pendientes de mi
desarrollo como profesional, saben que ahora estoy en un puesto de
*líder de proyecto* en un startup de telefonía en la nube. Además, quien
me conozca sabe que los temas *gerenciales* siempre me han dado ladilla,
como decimos acá en Venezuela.

El tema es que siempre, desde que tengo personal a mi cargo, me he
esforzado en hacerles entender que **no** trabajan para mi, **sino
conmigo**, es decir, **no soy** su jefe, sino alguien que los debe
ayudar a cumplir con sus obligaciones a tiempo y a quien le pueden
preguntar en caso de alguna duda, en fin, alguien quien puede
enseñarles, una especia de mentor o profesor o como quieran verlo. Para
mi, la definición de un líder, debe ser alguien a quien respetas
profesional y personalmente, alguien de quien consideras que puedes
aprender y a quien no le molesta aprender de ti. Eso, para mi, es un
líder.

Luego, está otro tema, uno que se remonta a cuando estaba estudiando en
la UCAB y discutía temas diversos con mi pana [Gerardo
Barcia](http://gerardobarcia.com/blog/){.reference .external}, gran
fanático de los temas que tienen que ver con Gerencia de Proyectos. Uno
de los temas que siempre causó controversia fue el del papel del
Ingeniero en un proyecto: gerente o ejecutor.

Siempre abogué por el papel del Ingeniero como *ejecutor* de un
proyecto, es decir si estudias *supuestamente* 5 años en una Universidad
y te enseñan a programar y diversos conceptos de sistemas operativos y
distribuidos, es para que los apliques en los sistemas que vas a
implementar o en los proyectos que vayas a desarrollar, además, en el
mundo de la informática se aplica mucho lo que yo llamo la *cultura
hacker*, que es el hecho de *respetar* a alguien que sabe más que tú y
que de verdad puedes comprobar que es así, es decir, alguien quien sabe
de qué habla y de quien puedes aprender o, quizás no, quizás es
simplemente alguien que está a tu mismo nivel pero que tiene las mismas
ganas que tu de aprender y tiene la misma *curiosidad científica* por
descubrir cosas y, quizás *reinventar la rueda* sólo para ver cómo
funciona.

Por otra parte, mi pana Gerardo siempre pensó que el rol del Ingeniero
en un proyecto es el del Gerente. Decirle a los técnicos qué hacer y
cómo hacerlo, estimar el tiempo que deberían tardarse, supervisarlos,
ver que lo que hacen cumple y aceptarlo o rechazarlo y dar directrices
de cómo y por dónde debe encaminarse el proyecto, es decir, el rol del
gerente de proyectos o, quizás, consultor.


## ¿Quién tenía la razón?

Pues creo que ambos, un Ingeniero en un proyecto debe cumplir ambos
roles, sin embargo para poder liderar un equipo, **siempre** he dicho
que debes tener experiencia suficiente como para poder decirle a alguien
lo que debe hacer con total confianza, es decir, poder tener la
habilidad, el conocimiento y la experiencia para conocer y prever
imprevistos dentro del proceso y advertir a la persona acerca de *con
qué se podría encontrar*.

El punto es que a mi pana Gerardo, le ha tocado programar, y programar
**duro**, al igual que a mi. Desarrollar sistemas en los que *alguien*
nos dice qué hacer y cuáles son las necesidades y debemos escribir
software que las cubra o que las automatice de la mejor manera posible y
sin poder tomar muchas decisiones acerca del *cómo* implementar las
cosas.

Ahora a mi, aunque considero, *quizás*, no tener la experiencia
necesaria, me toca liderar un equipo de programadores. Mi equipo me
respeta profesionalmente y nos llevamos **muy bien** personalmente, de
hecho, me parece que mi equipo está conformado por las mejores personas
que alguien podría querer, todos le ponen muchísimas ganas e incluso han
resuelto problemas en los que no tenía ni idea ni tiempo de revisar a
fondo cómo solventarlos, obviamente, todos estamos aprendiendo, ellos
porque aún están estudiando en la Universidad y el trabajo les sirve de
experiencia y yo porque es mi primer trabajo al frente de un equipo.

Como *persona a cargo* de un equipo, he metido la pata y **mucho**,
desde estimando tareas que toman más de lo que pensé, porque en realidad
no eran una tarea sino **muchas** que eran necesarias para cumplir un
objetivo, causando que quizás la persona se frustrara porque se tardó
mucho haciéndola, hasta viendo dependencias entre tareas que eran obvias
y asignándolas de manera errada con prioridades distintas y retrasando
algunas entregas, sí, lo se, **terrible**. Pero el colmo, y lo que me
hace escribir este artículo es que ocurrió algo en una reunión que me
hizo cuestionarme a mi mismo como líder de proyecto y como Ingeniero a
cargo de un proyecto:

Hace más o menos un mes y medio, se discutió el flujo de un proceso y
cómo debía ser implementado yo dije que debía ser de *X* forma y el CEO
dijo que debía ser de *Y* forma, al final, se hizo de la forma *Y*, pero
el tiempo demostró que la forma *X* era la correcta, entonces, tiempo y
esfuerzo *quizás* perdidos. No me duele mi tiempo y mi esfuerzo, sino el
de mi equipo porque fue **mi culpa** que las cosas no se hicieran bien
desde el principio, no se, capaz me faltó liderazgo o capacidad de
*algo* en el momento de la primera discusión, no lo se, el punto es que,
reflexionando, capaz producto de las bebidas espirituosas de hoy, he
empezado a pensar que quizás no estoy listo para asumir
responsabilidades de este calibre aún, quizás sea algo de actitud o
quizás sea algo de aptitud. Ustedes, ¿qué piensan?.
