Title: Carta abierta al DIPUTADO Leomagno Flores
Date: 2013-03-14
Author: Israel Fermín Montilla
Tags: política, software libre

Quienes me conocen saben que llevo un buen rato alejado del oscuro mundo
de la política o, más bien, politiquería venezolana. Las razones son muy
diversas, pero luego de ver el video que coloco a continuación,
simplemente no pude sino estallar en furia por motivos que expondré a lo
largo de este artículo:

<div class="youtube">

<iframe src="https://www.youtube.com/embed/KI5rYjd12iI" width="420" height="315" allowfullscreen seamless frameborder="0">
</iframe>

</div>

Les ruego que ven al video anterior antes de continuar la lectura

Ahora bien, antes de continuar, quiero explicar un poco mi posición en
cuando a la política. Todo aquel que me pregunta recibirá la misma
respuesta: *me criaron con muchos valores como para ser chavista, pero
tengo demasiada autoestima como para llamarme de oposición*,
declaraciones como esta, no hacen más que reafirmar mi postura aunque,
todos los que me conocen, saben que mis votos siempre van a *alguno* de
los dos lados.

Para mi, el problema de Venezuela no es de forma, sino de fondo. ¿Qué
quiero decir con esto?, no importa la forma que tenga el gobierno, si el
pueblo que está de fondo no cambia, no iremos a ningún lado ni
progresaremos jamás. Pero este es otra historia, ahora sí, a lo que
vine, decidí publicar esto por acá pues, a pesar de que busqué algún
correo electrónico por el cual comunicarme con la persona en cuestión,
no hay **ninguno** publicado, a pesar de que hay una ficha de contacto
en el portal de la Asamblea Nacional.

Señor Leomagno Flores, Diputado a la Asamblea Nacional y, además,
**Presidente de la Comisión Permanente de Ciencia, Tecnología e
Innovación**, permítame decirle que es usted un tremendo idiota, un
inepto y que luego de esa intervención debería **renunciar** por lo
menos a la comisión que preside porque dejó en evidencia el *poco*
conocimiento que tiene en el área, por no decir inexistente. Esto lo
digo con todo el respeto del mundo hacia su posición de Diputado ante la
Asamblea Nacional de la República Bolivariana de Venezuela, pero no
hacia su ignorancia como persona que, además, para tener el cargo que
tiene *debería* al menos haber leído un par de libros referentes a
tecnología y sus modelos de negocio y distribución, por no decir cosas
más técnicas como Ingeniería del Software, Protocolos de Comunicación y
Sistemas Operativos que no tiene, ciertamente, por qué conocer a
profundidad.

Ahora bien, mis palabras iniciales se que fueron algo fuertes, pero no
me cabe en la cabeza cómo alguien puede llamar *dictadura* al *Software
Libre*, y mucho menos, cómo puede decir que "uno pensaría que software
libre pudiera ser mucho software que la gente pudiera a su libre
albedrío tomar uno y no...". Pero bueno vamos por partes.

## El software libre es una dictadura

Señor Diputado, ¿sabía usted que existen licencias de software que
prohíben al licenciado hacer públicos los errores de programación que
encuentre en el producto?, es decir, te vendo una licencia, cara además,
para que uses algo que está *mal hecho* (porque esta programado por
humanos y los humanos nos equivocamos), pero además, si le dices a
alguien que tiene un error (que es de las cosas más normales en el mundo
de la computación), **puedo demandarte**. Si quisiera tener el *derecho*
a reportar bugs (o errores de programación), debo **pagar** un contrato
de soporte y reportarlo **directamente** a ellos y *rogarle a Dios* que
lo reparen para la versión que uso, sino, debo comprar la actualización.

Señor Diputado, ¿sabía usted que hay licencias de software que prohíben
que el licenciado ejerza alguna actividad de negocio en la que pudiera
competir con la compañía dueña del producto?, es decir, si yo compro un
programa de la empresa A, y el programa sirve para *hacer pastelitos* y
la empresa A, además de fabricar el programa, también *hace pastelitos*,
esa empresa podría *demandarme* por competir por ellos y usar su
producto.

Señor Diputado, ¿sabía usted que **ninguna empresa** que le venda un
programa bajo el esquema **privativo** va a permitirle a usted o a algún
especialista de su empresa/estado que lea, estudie o audite el código
fuente del software por el cual pagó?, eso es el equivalente a que usted
compre un carro y en el contrato que usted firma con el concesionario
diga que **es ilegal** que usted **abra el capot** y observe el motor,
aún cuando el automóvil deje de funcionar en plena autopista y usted
quiera intentar repararlo para seguir su camino, podrían **demandarlo**
por ello.

Sí, todo esto ocurre, han **cerrado** portales web y **demandado** a sus
dueños por publicar errores de seguridad (graves) en *Java*, por
ejemplo. Hay personas a las que las han hecho pagar \$5 para reportar un
bug en el *X* sistema operativo que no voy a mencionar, hay noticias en
todo internet de casos de errores **graves** de seguridad que las
compañías han dicho "está solucionado en la próxima versión" y todo esto
ocurre en el mundo del **Software Privativo**.

Si todo lo anterior no le suena a **dictadura**, entonces puede dejar de
leer aquí, sino, siga:

### Entonces, ¿qué es el Software Libre?

#### *Gratis no es libre, es una limosna* - Ernesto Hernández-Nóvich

El *Software Libre* es aquel que permite tres libertades fundamentales
que enumeraré a continuación:

-   **Libertad 0:** libertad de uso, es decir, debo poder utilizar el
    software para lo que yo quiera, sin importar si voy a competir o no.
    Puedo utilizarlo en mi casa, en mi empresa, vender una instalación o
    soporte sobre ese software, no importa, simplemente la licencia
    libre me permite hacer lo que desee con el software.
-   **Libertad 1:** libertad de estudiar cómo funciona por dentro el
    software, es decir, revisar el código fuente y, además, adaptarlo a
    mis necesidades particulares. De nada me sirve tener libertad de uso
    si al final el programa como viene *out of the box* no me sirve, por
    ello, debo poder modificarlo a mi gusto o según lo que me
    haga falta.
-   **Libertad 2:** libertad de hacer copias **sin ningún problema**, es
    decir, instalarlo en cuantos equipos quiera, sin necesidad de pagar
    *licencias extra* por hacerlo, esto, además, me permite distribuir
    copias del software **sin incurrir en delito alguno**. Si algo es
    bueno, queremos que más gente lo utilice.
-   **Libertad 3:** libertad para modificar/mejorar el software y
    distribuir las mejoras a la comunidad, si me beneficio de un
    desarrollo de la comunidad, estoy en la obligación moral de
    retribuirle con algo de mi trabajo y, de esta manera, ayudo a que el
    software evolucione y mejore cada día.

Respecto a esta última libertad, ¿sabía usted, señor Diputado, que Apple
basó el núcleo de su MacOS X en *FreeBSD*, un Sistema Operativo *libre*,
y no han liberado ni una sola línea de lo que hicieron con *Darwin* (el
núcleo de MacOS X)?. ¿Le parece esto justo?, ¿le parecen estas
libertades los preceptos de una *dictadura tecnológica*?.

Cuando hablo de *Software Libre* y las 4 libertades, me gusta muchísimo
incluir una quinta libertad, es quizás la más importante de todas: la
**Libertad de Elegir**, nadie me obliga a usar cierta herramienta, puedo
probarlas **todas** y quedarme con la que más me guste. Eso me lleva a
mi segundo punto.

### Hay poco Software Libre

Acá no puedo más que remitirme a la realidad. Si alguna vez ha visitado
una página web, ha usado **Software Libre** indirectamente. Muchas de
las páginas son servidas gracias a Apache o NGinx, dos de los servidores
web más populares de hoy. Una gran cantidad de las páginas dinámicas
están programadas en PHP, Python o Ruby, algunas en Perl, todos
lenguajes **libres**. Es también bastante probable que todo esto corra
sobre un servidor con sistema operativo Linux, también **libre**.

Sin ir muy lejos, este blog llega a ustedes gracias a Pelican y Python,
una herramienta y un lenguaje, ambos **Software Libre**.

Es poco lo que tengo que agregar acá, simplemente invitarles a colocar
*Linux distro list* o *alternativas de software libre* en Google y
evaluar ustedes mismos si hay poco *Software Libre*.

</div>

<div id="ahora-mi-reflexion" class="section">

## Ahora, mi reflexión

La política es uno de los campos más sucios que existen entre todas las
actividades del ser humano, por ello, todo lo que se politiza generará
discordia. Para mi, el gobierno cometió un error al politizar acerca del
*Software Libre*. La dinámica de la *Asamblea Nacional* Venezolana puede
resumirse en: *Si eres de mi partido, aplaudo todo lo que digas, sin
importar que sea la idiotez más grande que haya escuchado, debo
apoyarte. Pero si eres del contrario, debo llevarte la contraria y decir
que lo que dices está mal, sin importar que sea la mejor idea de los
últimos 20 años*.

Así se maneja el debate político en Venezuela y así se manejará
cualquier cosa que se politice, las leyes de infogobierno deberían ser
discutidas con Ingenieros, Programadores y Especialistas en tecnología
o, al menos, deberían ser consultados pues como hemos visto en la
discusión intervienen entes que no saben de informática y, por lo que
han demostrado en los últimos años, mucho menos saben gobernar.

Los dejo ahora con una charla que dio Ernesto Hernández-Nóvich acerca
del *Estado* y el *Software Libre*, **La libertad no admite grises**

<div class="youtube">

<iframe src="https://www.youtube.com/embed/Jukl9gBvSS8" width="420" height="315" allowfullscreen seamless frameborder="0">
</iframe>
