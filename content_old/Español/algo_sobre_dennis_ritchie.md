Title: Algo sobre Dennis Ritchie
Date: 2011-10-18
Author: Israel Fermín Montilla
Tags: reflexion, personal, Denis Ritchie

Quizás no sepas de quien se trata, es probable que hayas escuchado su
nombre alguna vez si (¿y sólo si?) estudias o estudiaste computación,
informática o alguna carrera similar.

Aún así, si no eres computista, seguramente utilizas algo basado en
alguno de sus trabajos, sin importar que utilices Windows, Linux o Mac
OS, estás aprovechando algo del legado de este genio. Dennis Ritchie
falleció este 12 de octubre, a la edad de 70 años. Una semana despues de
que perdiéramos a otra mente brillante, pero del mundo de los negocios:
el Sr. Steve Jobs. Ritchie, quien era PhD en Ciencias de la Computación,
fue uno de los desarrolladores principales del sistema operativo UNIX y
el diseñador del Lenguaje de Programación C, co-autor del libro "The C
Programming Language" junto con Brian Kernighan, mejor conocido como
"The K&R Book", uno de los mejores textos de referencia acerca del
lenguaje. Si eres usuario de UNIX, Linux o Mac OS, estás utilizando algo
que se basa en uno de sus trabajos, de hecho, en ambos. Galardonado en
1999 con el Premio Nacional de Tecnología en los Estados Unidos, ganador
del Turing de 1983, autor de múltiples publicaciones en el campo de los
Lenguajes de Programación y las Ciencias de la Computación en general,
investigador como forma de vida, realmente hemos perdido a una de las
mentes más brillantes del siglo XX.
